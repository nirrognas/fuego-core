# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os, sys

# Search for plot.data files
search_root = '/fuego-core/scripts/parser/tests/'

for root, dirs, files in os.walk(search_root):
    for file in files:
        if not file.endswith('.data'):
            continue
        print os.path.join(root, file)
        with open(os.path.join(root, file)) as f:
            build_number = 0 # init
            num_duplicates = 0 # duplicate counter
            prev_num_duplicates = 0
            for line in f.readlines():
                if build_number == int(line.split()[3]):
                    num_duplicates += 1
                else:
                    if (prev_num_duplicates != num_duplicates) and (num_duplicates != 0) and (prev_num_duplicates != 0):
                            print "Build number counter error for build "+str(build_number)+".\tPrevious number = "+str(prev_num_duplicates)+".\tCurrent number = "+str(num_duplicates)
                    prev_num_duplicates = num_duplicates
                    num_duplicates = 1
                    if (build_number+1 != int(line.split()[3])) and (build_number != 0):
                            print "Wrong build order. "
                    build_number = int(line.split()[3])
