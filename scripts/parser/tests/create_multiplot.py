# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import os, re, sys
sys.path.insert(0, '/home/jenkins/parsers/common')
import common as plib

plib.JOB_NAME='testjob'
plib.TOOLCHAIN='none'
plib.TARBALL='empty'
plib.BUILD_ID=0
plib.BUILD_NUMBER=200
plib.BUILDS_NUM=100
plib.REF_LOG='ref.log'
plib.CUR_LOG='cur.log'

# Data file test cases
#c1: Missing file - PASSED
#c2: testCorrectData.data - PASSED
#c3: testduplicatedData.data - PASSED
#c4: testEmptyData.data - PASSED
#c5: testMissingResultsData.data - PASSED
#c6: testPartiallyMissingData.data - PASSED
#c7: testGarbageData.data - PASSED
#c8: testCorruptedData.data - PASSED

plib.PLOT_DATA = 'testCorrectData.data'
plib.PLOT_FILE = 'test_plot.png'

plib.create_multiplot('testLabel')
