#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017 Toshiba corp.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
common.py - This library contains parsing functions
By Daniel Sangorrin (July 2017)
"""

import sys, os, re, json, time, collections
from fuego_parser_utils import hls, split_test_id, get_test_case

loglevel = "info"
cur_area = "parser"
def set_loglevel(area):
    global loglevel, cur_area

    cur_area = area

    # if env var not defined, use 'info' default for all areas
    loglevels = os.environ.get('FUEGO_LOGLEVELS',
        "parser:info,criteria:info,charting:info")
    loglevel = 'info'
    for ll_area in loglevels.split(","):
        if ll_area.startswith(area):
            if ':' in ll_area:
                loglevel=ll_area.split(':')[1]
            else:
                eprint("Bad syntax in FUEGO_LOGLEVELS")

    # handle compatibility with old FUEGO_DEBUG flag
    # FIXTHIS - remove this at version 1.6
    bitmask = {"parser":2, "criteria":4, "charting":8}.get(area, 0)
    if bitmask:
        try:
            if int(os.environ['FUEGO_DEBUG']) & bitmask:
                loglevel = "debug"
                dprint("Fuego %s debug messages active" % area)
        except:
            pass

    #print("LOGLEVEL debug: area=%s, set_loglevel=%s" % (cur_area, loglevel))

def dprint(msg):
    if loglevel == "debug":
        print("DEBUG(" + cur_area + '): ' + msg)

def vprint(msg):
    if loglevel in ["debug", "verbose"]:
        print(msg)

def iprint(msg):
    if loglevel in ["debug", "verbose", "info"]:
        print(msg)

def wprint(msg):
    if loglevel in ["debug", "verbose", "info", "warning"]:
        print("### " + msg)

# always print errors
def eprint(msg):
    print("!!! ERROR: " + msg)

# environment variables used by the parser
try:
    FUEGO_HOST=os.environ['FUEGO_HOST']
except:
    FUEGO_HOST="localhost"

try:
    FUEGO_BATCH_ID=os.environ['FUEGO_BATCH_ID']
except:
    FUEGO_BATCH_ID="none"

try:
    TESTPLAN=os.environ['TESTPLAN']
except:
    TESTPLAN="none"

# FIXTHIS: use an ENV_ prefix and capital letters for all environment variables
env_list = ['FUEGO_RW', 'FUEGO_RO', 'FUEGO_CORE', 'NODE_NAME', 'TESTDIR', 'TEST_HOME',
        'TESTSPEC', 'BUILD_NUMBER', 'BUILD_ID', 'BUILD_TIMESTAMP',
        'TOOLCHAIN', 'FWVER', 'LOGDIR', 'FUEGO_START_TIME', 'Reboot',
        'Rebuild', 'Target_PreCleanup', 'WORKSPACE', 'JOB_NAME',
        'FUEGO_VERSION', 'FUEGO_CORE_VERSION', 'TESTSUITE_VERSION',
        'Target_PostCleanup'
        ]

# add certain environment variables to this module
_g = globals()
for env_var in env_list:
    _g[env_var] = os.environ[env_var]

# main files used by the parser
REF_JSON  = TEST_HOME + '/reference.json'
CHART_CONFIG_JSON  = TEST_HOME + '/chart_config.json'
TEST_LOG = '%s/logs/%s/%s.%s.%s.%s/testlog.txt' % (FUEGO_RW, TESTDIR, NODE_NAME, TESTSPEC, BUILD_NUMBER, BUILD_ID)
RUN_JSON = LOGDIR + '/run.json'

#Here are some pre-packaged regex strings
# m[0] = numer, m(1) = status
REGEX_TEST_NUMBER_STATUS = '"^TEST-(\d+) (.*)$'

# used by test's parser.py
def parse_log(regex_string):
    set_loglevel("parser")
    vprint("Parsing " + TEST_LOG)
    dprint("Using regular expression string: " + regex_string)

    regex = re.compile(regex_string, re.MULTILINE)
    return parse(regex)

# this is the old parse routine, that takes a compiled regex instance
# it is kept separate for compatiblity with old Fuego and JTA test jobs
# it should be eliminated (merge with parse_log) in Fuego version 1.6
def parse(regex):
    set_loglevel("parser")
    try:
        dprint("Reading test log file %s" % TEST_LOG)
        test_log = open(TEST_LOG, 'r')
    except IOError:
        eprint("Can't open " + TEST_LOG)
        matches = None
        return matches

    try:
        if loglevel != "debug":
            data = test_log.read()
        else:
            lines = test_log.readlines()
    except IOError:
        eprint("Can't read " + TEST_LOG)
        matches = None
        return matches

    if loglevel != "debug":
        matches = regex.findall(data)
    else:
        matches = []
        for line in lines:
            dprint("line="+line[:-1])
            m = regex.search(line)
            if m:
                g = m.groups()
                if len(g)==1:
                    g = g[0]
                dprint("match groups=" + str(g))
                matches.append(g)
            else:
                dprint("no match")

    test_log.close()

    dprint("matches: " + str(matches))
    return matches

def add_results(results, run_data):
    dprint("in add_results")
    if not results:
        return
    for test_case_id in results.keys():
        test_case = get_test_case(test_case_id, run_data)
        if not test_case:
            continue
        # Benchmark case: array of measurements
        if isinstance(results[test_case_id], list):
            for result in results[test_case_id]:
                for measure in test_case['measurements']:
                    if measure['name'] == result['name']:
                        measure['measure'] = result['measure']
        # Functional case: status string
        elif isinstance(results[test_case_id], str):
            test_case['status'] = results[test_case_id]
        else:
            wprint("Unrecognized results format")

def init_run_data(run_data, ref):
    run_data['test_sets'] = ref['test_sets']

    for test_set in run_data['test_sets']:
        test_set['status'] = 'SKIP'
        for test_case in test_set['test_cases']:
            if 'status' not in test_case:
                # only set to SKIP if the parser.py hasn't already
                test_case['status'] = 'SKIP'
            measurements = test_case.get('measurements', [])
            for measure in measurements:
                measure['status'] = 'SKIP'

def get_criterion(tguid, criteria_data, default_criterion=None):
    criterion = default_criterion
    criteria = criteria_data.get('criteria', [])
    for crit in criteria:
        if crit['tguid'] == tguid:
            criterion = crit
    return criterion

def data_compare(value, ref_value, op):
    try:
        if op == 'lt':
            result = float(value) < float(ref_value)
        elif op == 'le':
            result = float(value) <= float(ref_value)
        elif op == 'gt':
            result = float(value) > float(ref_value)
        elif op == 'ge':
            result = float(value) >= float(ref_value)
        elif op == 'eq':
            result = float(value) == float(ref_value)
        elif op == 'ne':
            result = float(value) != float(ref_value)
        elif op == 'bt':
            ref_low, ref_high = ref_value.split(',', 1)
            result = float(value) >= float(ref_low) and float(value) <= float(ref_high)
        elif op == 'none':
            result = True
        else:
            return "ERROR"
    except:
        return "SKIP"

    if result:
        status = "PASS"
    else:
        status = "FAIL"

    dprint("  result=%s" % result)
    dprint("  status=%s" % status)
    return status

def check_measure(tguid, criteria_data, measure):
    dprint("in check_measure")
    value = measure.get('measure', None)
    dprint("  tguid='%s'" % tguid)
    dprint("  value='%s'" % value)

    if value == None:
        eprint("No value in check_measure - returning SKIP")
        return 'SKIP'

    criterion = get_criterion(tguid, criteria_data)
    dprint("  criterion='%s'" % criterion)
    if not criterion:
        dprint("No criterion: status='PASS'")
        return 'PASS'

    try:
        reference = criterion["reference"]
    except:
        eprint("criteria (%s) missing reference - returning SKIP" % criterion)
        return 'SKIP'

    dprint("reference=%s" % str(reference))

    # I had ref_value = reference.get('value', None) here, and it didn't work
    # if you change this, be sure to test it
    try:
        ref_value = reference["value"]
    except:
        eprint("criteria (%s) missing reference value - returning SKIP" % criterion)
        return 'SKIP'

    try:
        op = reference["operator"]
    except:
        eprint("criteria (%s) missing reference operator - returning SKIP" % criterion)
        return 'SKIP'

    return data_compare(value, ref_value, op)

def decide_status(tguid, criteria_data, child_pass_list, child_fail_list):
    dprint("in decide_status:")
    dprint("    tguid=%s" % tguid)

    pass_count = len(child_pass_list)
    fail_count = len(child_fail_list)
    dprint("    pass_count=%s" % pass_count)
    dprint("    fail_count=%s" % fail_count)

    # default criterion is pass unless a single test element fails
    default_criterion = {
        'tguid': tguid,
        'max_fail': 0
    }
    criterion = get_criterion(tguid, criteria_data, default_criterion)

    vprint("Applying criterion " + str(criterion))

    must_pass_list = criterion.get('must_pass_list', [])
    fail_ok_list = criterion.get('fail_ok_list', [])

    min_pass = criterion.get('min_pass', None)
    if not min_pass and must_pass_list:
        min_pass = len(must_pass_list)

    have_max_fail=criterion.has_key("max_fail")
    if have_max_fail:
        max_fail = criterion.get('max_fail')
    elif len(fail_ok_list)>0:
        have_max_fail = True
        max_fail = len(fail_ok_list)
    else:
        max_fail = 0

    if (pass_count == 0) and (fail_count == 0):
        status = "SKIP"
    else:
        dprint("Default '%s' to PASS result" % tguid)
        status = "PASS"

    # the order of the following tests is important
    if min_pass:
        if pass_count < min_pass:
            dprint("Fail '%s' because pass count (%d) < min_pass (%d)" % (tguid, pass_count, min_pass))
            status = "FAIL"
        else:
            dprint("Leaving '%s' as PASS because pass count (%d) >= min_pass (%d)" % (tguid, pass_count, min_pass))

    if have_max_fail:
        if fail_count > max_fail:
            dprint("Fail '%s' because fail count (%d) > max_fail (%d)" % (tguid, fail_count, max_fail))
            status = "FAIL"
        else:
            dprint("Leaving '%s' as PASS because fail count (%d) <= max_fail (%d)" % (tguid, fail_count, max_fail))

    # make sure required children passed
    if must_pass_list:
        for required_obj in must_pass_list:
            if required_obj not in child_pass_list:
                dprint("Fail '%s' because %s failed, but is on must_pass_list" % (tguid, required_obj))
                status = "FAIL"
                break

    # fail test if there's an explicit failok list
    # and a child failed that's not on it

    # NOTE: it doesn't make much sense to use both max_fail
    # and failok_list, but in any case the criteria file
    # should have max_fail >= len(failok_list)
    if fail_ok_list:
        for fail_obj in child_fail_list:
            if fail_obj not in fail_ok_list:
                dprint("Fail '%s' because %s failed and is not in fail_ok_list" % (tguid, fail_obj))
                status = "FAIL"
                break

    dprint("Result for '%s' is '%s'" % (tguid, status))
    return status

def split_old_id(old_id):
    # measure at end, test_set at front, test_case in middle
    # test_set and test_case can be empty, and if so are converted
    # to "default", and <test_name>, respectively

    test_name = TESTDIR.split(".")[1]
    test_set = "default"
    test_case = test_name

    if "." not in old_id:
        measure = old_id
    else:
        part1, measure = old_id.rsplit(".", 1)
        if "." not in part1:
             test_case = part1
        else:
             test_set, test_case = part1.split(".", 1)
    return test_set, test_case, measure

def convert_reference_log_to_criteria(filename):
    test_name = TESTDIR.split(".")[1]
    vprint("Converting reference.log to criteria data for test %s" % test_name)

    lines = open(filename).readlines()
    i = 0
    crit_list = []
    while i < len(lines):
        line = lines[i][:-1]
        if lines[i].startswith("["):
            # remove brackets
            crit_str = line[1:line.find("]")]

            old_measure_id, operation = crit_str.split("|")
            ts_name, tc_name, measure_name = split_old_id(old_measure_id)
            tguid = "%s.%s.%s" % (ts_name, tc_name, measure_name)
            value = lines[i+1].strip()

            reference = {"value":value, "operator": operation.strip()}
            crit = {"tguid":tguid, "reference": reference }
            crit_list.append(crit)
            i += 2
        else:
            i += 1

    criteria_data = {"schema_version":"1.0", "criteria": crit_list }
    dprint("criteria_data='%s'" % criteria_data)
    return criteria_data

def load_criteria():
    # determine if there's a board-specific override for the criteria file
    default_criteria_filename = TEST_HOME + '/criteria.json'
    ro_board_crit_filename = "%s/boards/%s-%s-criteria.json" % (FUEGO_RO, NODE_NAME, TESTDIR)
    rw_board_crit_filename = "%s/boards/%s-%s-criteria.json" % (FUEGO_RW, NODE_NAME, TESTDIR)

    # user-specified path takes precedence
    if os.environ.has_key('FUEGO_CRITERIA_JSON_PATH'):
        criteria_filename = os.environ['FUEGO_CRITERIA_JSON_PATH']
    elif os.path.exists(ro_board_crit_filename):
        criteria_filename = ro_board_crit_filename
    elif os.path.exists(rw_board_crit_filename):
        criteria_filename = rw_board_crit_filename
    else:
        criteria_filename = default_criteria_filename

    if criteria_filename != default_criteria_filename:
        vprint("Using criteria file: %s" % criteria_filename)

    if os.path.exists(criteria_filename):
        try:
            with open(criteria_filename) as criteria_file:
                criteria_data = json.load(criteria_file, object_pairs_hook=collections.OrderedDict)
        except IOError:
            eprint("could not load criteria file (%s)" % criteria_filename)
            sys.exit(1)
        except:
            eprint("faulty criteria file (%s)" % criteria_filename)
            sys.exit(2)

    else:
        vprint("Missing criteria.json file (%s), looking for reference.log" % criteria_filename)
        reference_log_filename = TEST_HOME + '/reference.log'
        if os.path.exists(reference_log_filename):
            criteria_data = convert_reference_log_to_criteria(reference_log_filename)
        else:
            # no reference log, generate stubs
            vprint("No reference.log found. Using default criteria.")
            criteria_data = {"schema_version": "1.0", "criteria": [] }

    dprint("criteria_data from load_criteria()='%s'" % criteria_data)
    return criteria_data

def apply_criteria(run_data, criteria_data):
    set_loglevel("criteria")
    dprint("in apply_criteria")

    test_set_pass_list = []
    test_set_fail_list = []
    for test_set in run_data['test_sets']:
        ts_name = test_set['name']
        dprint("checking test_set '%s'" % ts_name)
        test_case_pass_list = []
        test_case_fail_list = []
        for test_case in test_set['test_cases']:
            tc_name = test_case['name']
            dprint("checking test_case '%s.%s'" % (ts_name, tc_name))
            test_case_tguid = ts_name + '.' + tc_name
            if 'measurements' in test_case:
                measure_pass_list = []
                measure_fail_list = []
                for measure in test_case['measurements']:
                    m_name = measure['name']
                    measure_tguid = ts_name + '.' + tc_name + '.' + m_name
                    dprint("checking measurement '%s'" % measure_tguid)
                    measure['status'] = check_measure(measure_tguid,
                                                        criteria_data,
                                                        measure)
                    if measure['status'] == 'FAIL':
                        measure_fail_list.append(m_name)
                    elif measure['status'] == 'PASS':
                        measure_pass_list.append(m_name)
                    dprint("measure '%s' status=%s" % (measure_tguid, measure['status']))
                test_case['status'] = decide_status(test_case_tguid,
                                                    criteria_data,
                                                    measure_pass_list,
                                                    measure_fail_list)
            if test_case['status'] == 'FAIL':
                test_case_fail_list.append(tc_name)
            elif test_case['status'] == 'PASS':
                test_case_pass_list.append(tc_name)
            dprint("test_case '%s' status=%s" % (test_case_tguid, test_case['status']))
        test_set['status'] = decide_status(ts_name,
                                           criteria_data,
                                           test_case_pass_list,
                                           test_case_fail_list)
        dprint("test_set '%s' status=%s" % (ts_name, test_set["status"]))
        if test_set['status'] == 'FAIL':
            test_set_fail_list.append(ts_name)
        elif test_set['status'] == 'PASS':
            test_set_pass_list.append(ts_name)
    run_data['status'] = decide_status(run_data['name'],
                                       criteria_data,
                                       test_set_pass_list,
                                       test_set_fail_list)
    dprint("test '%s' status=%s" % (run_data["name"], run_data["status"]))
    return run_data['status']

def create_default_ref(results):
    dprint("in create_default_ref")
    ref = {'test_sets': []}
    for test_case_id in results.keys():
        test_set_name, test_case_name = split_test_id(test_case_id)
        item = results[test_case_id]
        if isinstance(item, list):
            test_case = {
                'name': test_case_name,
                'measurements': item
            }
        else:
            test_case = {
                'name': test_case_name,
                'status': item
            }

        test_set = next((item for item in ref['test_sets'] if item['name'] == test_set_name), None)
        if not test_set:
            test_set = {'name': test_set_name, 'test_cases': [test_case]}
            ref['test_sets'].append(test_set)
        else:
            test_set['test_cases'].append(test_case)

    return ref

def name_compare(a, b):
    try:
        a_name = a["name"]
    except:
        a_name = a
    try:
        b_name = b["name"]
    except:
        b_name = b
    return cmp(a_name, b_name)

def dump_ordered_data(data, indent=""):
    if type(data)==type({}):
        print "%s{" % indent
        keylist = data.keys()
        keylist.sort()
        for key in keylist:
            print '%s "%s":' % (indent+"  ", key),
            dump_ordered_data(data[key],indent+"    ")
        print "%s}" % indent
        return
    if type(data)==type([]):
        print "%s[" % indent
        item_list = data[:]
        item_list.sort(name_compare)
        for item in item_list:
            dump_ordered_data(item,indent+"    ")
        print "%s]" % indent
        return
    print "%s%s" % (indent, data)

def dprint_data(label, data):
    if loglevel == "debug":
        dprint("Dump of '%s':" % label)
        dump_ordered_data(data)

def prepare_run_data(results, criteria_data):
    dprint("in prepare_run_data")
    duration_ms = int(time.time())*1000 - int(FUEGO_START_TIME)
    run_data = {
        "schema_version":"1.0",
        "name":TESTDIR,
        "duration_ms":duration_ms,
        "metadata":{
            "fuego_version":FUEGO_VERSION,
            "fuego_core_version":FUEGO_CORE_VERSION,
            "testsuite_version":TESTSUITE_VERSION,
            "host_name":FUEGO_HOST,
            "board":NODE_NAME,
            "compiled_on":"docker", #FIXTHIS
            "job_name":JOB_NAME,
            "kernel_version":FWVER,
            "toolchain":TOOLCHAIN,
            "start_time":FUEGO_START_TIME,
            "timestamp":BUILD_TIMESTAMP,
            "test_plan":TESTPLAN,
            "test_spec":TESTSPEC,
            "build_number":BUILD_NUMBER,
            "batch_id":FUEGO_BATCH_ID,
            "keep_log":True, #FIXTHIS
            "reboot":Reboot,
            "rebuild":Rebuild,
            "target_precleanup":Target_PreCleanup,
            "target_postcleanup":Target_PostCleanup,
            "workspace":WORKSPACE,
            "attachments":[
                {
                    "name":"devlog",
                    "path":"devlog.txt"
                },
                {
                    "name":"snapshot",
                    "path":"machine-snapshot.txt"
                },
                {
                    "name":"syslog.before",
                    "path":"syslog.before.txt"
                },
                {
                    "name":"syslog.after",
                    "path":"syslog.after.txt"
                },
                {
                    "name":"testlog",
                    "path":"testlog.txt"
                },
                {
                    "name":"build_xml",
                    "path":"build.xml"
                },
                {
                    "name":"consolelog",
                    "path":"consolelog.txt"
                },
                {
                    "name":"test_spec",
                    "path":"spec.json"
                },
                {
                    "name":"prolog",
                    "path":"prolog.sh"
                }
            ]
        }
    }

    # read tests/<test>/reference.json
    try:
        with open(REF_JSON) as ref_file:
            ref = json.load(ref_file, object_pairs_hook=collections.OrderedDict)
    except:
        vprint("No reference.json found (%s)" % REF_JSON)
        ref = create_default_ref(results)
        dprint("ref=%s" % ref)

    dprint_data("run_data", run_data)
    dprint("calling init_run_data to populate run_data with reference info")

    # make an empty file (possibly containing units)
    init_run_data(run_data, ref)

    dprint_data("run_data", run_data)
    dprint("adding results to run_data")

    # add the results for this run
    add_results(results, run_data)

    dprint_data("run_data", run_data)
    dprint("applying criteria to run_data")

    # add the criteria-generated results (for test_sets, benchmark results, etc.)
    apply_criteria(run_data, criteria_data)

    dprint_data("run_data", run_data)
    return run_data

def delete(data, key):
    if key in data:
        del(data[key])

def save_run_json(run_data):
    try:
        vprint("Writing run data to " + RUN_JSON)
        with open(RUN_JSON, 'w') as f:
            f.write(json.dumps(run_data, sort_keys=True, indent=4, separators=(',', ': ')))
        # FIXTHIS: add JSON schema validation
    except:
        hls("Problems dumping to " + RUN_JSON, "e")

# this is the main routine to process test results
def process(results={}):
    """ results: dict that maps a test_case_id (test_set.test_case) to a status
        string or an array of measurements.
        each result can be a singleton or a list of measures
        e.g.: results['test_set1.test_casea'] = "PASS"
        e.g.: results['Sequential_Output.Block'] = [{'name':'speed', 'measure':123}, {'name':'cpu', 'measure':78}]
    """
    # TRB 2018-10-05: the results.json file is not currently used
    #from fuego_parser_results import update_results_json

    from prepare_chart_data import store_flat_results, make_chart_data

    dprint("parsed results: " + str(results))

    set_loglevel("criteria")
    criteria_data = load_criteria()
    run_data = prepare_run_data(results, criteria_data)
    save_run_json(run_data)
    test_logdir = FUEGO_RW + '/logs/' + TESTDIR

    # TRB 2018-10-05: the results.json file is not currently used
    #update_results_json(test_logdir, TESTDIR, REF_JSON)

    set_loglevel("charting")
    data_lines = store_flat_results(test_logdir, run_data, criteria_data)
    make_chart_data(test_logdir, TESTDIR, CHART_CONFIG_JSON, data_lines)

    status = run_data.get('status', 'FAIL')
    dprint("status=%s" % status)
    return 0 if status == 'PASS' else 1

# compatibility routine for process_data
# convert legacy arguments into arguments for process()

# Assume the last segment of each result id is the measure name, and stuff
# leading up to it is the test_case_id (see split_old_id)

# NOTE: This routine is only used by old-style Benchmark parser.py modules
def process_data(ref_section_pat, test_results, plot_type, label):
    dprint("in process_data")
    measurements = {}

    test_name = TESTDIR.split(".")[1]

    # convert old-style cur_dict into measurements structure
    for (old_id, value) in test_results.items():
        ts_name, tc_name, measure = split_old_id(old_id)
        test_case_id = "%s.%s" % (ts_name, tc_name)
        new_measure = {"name":measure, "measure": float(value)}
        if not measurements.has_key(test_case_id):
            measurements[test_case_id] = [new_measure]
        else:
            measurements[test_case_id].append(new_measure)

    return process(measurements)

def make_dirs(dir_path):
    if os.path.exists(dir_path):
        return

    try:
        os.makedirs(dir_path)
    except OSError:
        pass

def close_log(out_fd):
    if out_fd:
        out_fd.close()

# return file descriptor for a new per-testcase log file
# the output directory and log file name is determined from
# the testcase name.
def open_new_log(result_dir, test_index, testcase_names):
    out_fd = None

    dprint("open_new_log: test_index=%d" % test_index)

    # determine testcase name and test_set, based on the index
    test_set = "default"
    if test_index < 0:
        testcase_filename = "test_start"
    elif test_index < len(testcase_names):
        full_tc_name = testcase_names[test_index]

        # slashes are not allowed in the file or directory name
        full_tc_name = full_tc_name.replace("/","_")

        parts = full_tc_name.split(".")
        test_set = ".".join(parts[:-1])
        testcase_filename = parts[-1]
    else:
        testcase_filename = "test_end"

    dprint("open_new_log: new log file = %s/%s.log" %
            (test_set, testcase_filename))
    # create directory and log file
    try:
        out_dir = result_dir + '/%s/outputs' % test_set
        make_dirs(out_dir)
        testcase_filepath = out_dir+"/%s.log" % testcase_filename
        out_fd = open(testcase_filepath, "w")
    except IOError:
        print("Cannot create '%s', or '%s' cannot be opened." %
                (out_dir, testcase_filepath))

    return(out_fd)


def split_output_per_testcase (regex_string, measurements, info_follows_regex=0):
    '''
        For this Functional test, there is a testlog.txt file
        that contains the output log of each testcase. This function
        splits output.log into a log file for each testcase.
    '''
    # prepare top output directory
    result_dir = '%s/logs/%s/%s.%s.%s.%s/result' % \
            (FUEGO_RW, TESTDIR, NODE_NAME, TESTSPEC, BUILD_NUMBER, BUILD_ID)
    make_dirs(result_dir)

    # open input
    try:
        fd = open(TEST_LOG)
    except IOError:
        print("Cannot open log file '%s'" % TEST_LOG)
        return

    lines = fd.readlines()
    fd.close()

    # note that measurements is an OrderedDict, so keys comes out ordered
    testcase_names = measurements.keys()

    if info_follows_regex:
        # You can have stuff before the first testcase delimiter that
        #   is not part of any testcase data
        # It is put into the file 'default/outputs/test_start.log'
        test_index = -1
    else:
        # info precedes regex:
        # data from the very first line of the testlog goes into
        # the log for the first testcase
        # You can have stuff after the last testcase delimiter that
        #   is not part of any testcase data
        # It is put into the file 'default/outputs/test_end.log'
        test_index = 0

    out_fd = open_new_log(result_dir, test_index, testcase_names)
    test_index += 1

    re_pat = re.compile(regex_string)

    for line in lines:
        # check for testcase delimiter line
        m = re_pat.match(line)
        if m:
            dprint("split_output_per_testcase: pattern match on line: '%s'" % line[:-1])
            # if we match the regex, then we know that either:
            # 1. this is the last line of the testcase (not info_follows_regex)
            # 2. this is the first line of new testcase (info_follows_regex)
            # no matter which case, we close the current log file and
            # start a new one.

            if not info_follows_regex:
                # this is the last line of testcase info
                # save it to the current log, and open a new log
                if out_fd:
                   out_fd.write(line)
                close_log(out_fd)
                out_fd = open_new_log(result_dir, test_index, testcase_names)
                test_index += 1
                # loop to get next line into the new log file
                continue
            else:
                # this is the first line of testcase info
                # close the previous log, if any and open a new log
                close_log(out_fd)
                out_fd = open_new_log(result_dir, test_index, testcase_names)
                test_index += 1
                # drop through to write current line into new log

        # save each line to the current per-testcase log
        if out_fd:
            out_fd.write(line)

    # after the loop, close any still-open log file
    close_log(out_fd)


def main():
    pass

if __name__ == '__main__':
    main()

