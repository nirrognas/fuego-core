#!/bin/bash
# vim: ts=2 sw=2 et :
# need_check - module with need_check function for fuego
#
# NEED_xxx variables are declared in the base test script
# the need_check function processes all of them and aborts the test
# if something that is needed is missing
#
#
###### examples
# memory can have a suffix of G, M, or K (or have no suffix)
#NEED_MEMORY=1G

# Free storage should specify a directory where the storage is needed
# as a second argument
#TESTDIR="/home"
#NEED_FREE_STORAGE="1M $TESTDIR"

# kernel configuration options are specified as a list
#NEED_KCONFIG="CONFIG_PRINTK=y CONFIG_LOCKDEP_SUPPORT=n CONFIG_ARM"

# root permissions are required for test
#NEED_ROOT=1

ONE_K=1024
ONE_M=1048576
ONE_G=1073741824
ONE_T=1099511627776

# check for module presence
#NEED_MODULE=msdos

# $1 has a string to convert to bytes
function to_bytes {
  str=$1
  value="${str%%[a-zA-Z]*}"

  case $str in
    *T)
      echo $(( value * $ONE_T ))
      ;;
    *G)
      echo $(( value * $ONE_G ))
      ;;
    *M)
      echo $(( value * $ONE_M ))
      ;;
    *K)
      echo $(( value * $ONE_K ))
      ;;
    *)
      echo $value
      return 1
  esac
  return 0
}

function get_memory {
  if [ -z "$HAVE_MEMORY" ] ;  then
    gm_board_tmpfile=$(mktemp)
    gm_host_tmpfile=$(mktemp)
    cmd "cat /proc/meminfo >$gm_board_tmpfile"
    get $gm_board_tmpfile $gm_host_tmpfile
    memory_kb=$(cat $gm_host_tmpfile | grep MemFree | sed -e "s/.*:[ ]*\(.*\)kB/\1/")
    #rm $tmpfile
    result=$(( memory_kb * $ONE_K ))
    ftc set-var -b $NODE_NAME HAVE_MEMORY=$result
    cmd "rm $gm_board_tmpfile"
    rm $gm_host_tmpfile
    echo $result
  else
    echo $HAVE_MEMORY
  fi
}

# $1 has the amount of memory needed
function check_memory {
  need_mem_bytes=$(to_bytes "$1")
  echo "need_mem_bytes=${need_mem_bytes}"
  have_mem_bytes=$(get_memory)
  echo "have_mem_bytes=${have_mem_bytes}"

  if [ $have_mem_bytes -gt $need_mem_bytes ] ; then
    echo "OK:    have the needed bytes!!"
    return 0
  else
    echo "Error: don't have enough bytes"
    return 1
  fi
}

function test_parse_storage {
  # here's a neat shell trick to split input on whitespace
  # suppress globbing during the 'set'
  echo "in test parse storage: arg1=[$1]"
  set -f
  arg_array=($1)

  echo "in test parse storage: arg_array=[${argarray[@]}]"

  size="${arg_array[0]}"
  dir="${arg_array[1]}"
  set +f

  echo "in test parse storage: size=$size"
  echo "in test parse storage: dir=$dir"
}

# $1 = a directory to test the free storage of
# df returns:
# Filesystem Total Used Available Use_percent Mount_point
function get_free_storage {
  #if [ -z "$HAVE_FREE_STORAGE" ] ;  then
    gfs_board_tmpfile=$(mktemp)
    gfs_host_tmpfile=$(mktemp)
    cmd "df $1 >$gfs_board_tmpfile"
    get $gfs_board_tmpfile $gfs_host_tmpfile
    storage_line=$(cat $gfs_host_tmpfile | tail -1)
    storage_array=($storage_line)
    storage_kb=${storage_array[3]}
    result=$(( storage_kb * $ONE_K ))
    cmd "rm $gfs_board_tmpfile"
    rm $gfs_host_tmpfile
  # else
  #    result="$HAVE_FREE_STORAGE"
  echo $result
}

# This function detects the Linux distribution used on the target board and
# exports the following variables:
#    BOARD_DISTRO_ID (e.g. ubuntu, debian, centos)
#    BOARD_DISTRO_VERSION_ID (e.g. "16.04", "8", "7")
#
# If the distribution cannot be automatically detected it will return 1.
# Defining BOARD_DISTRO_ID and BOARD_DISTRO_VERSION_ID on the board file
# disables automatic detection.
function check_distro {
    if [ -z "$BOARD_DISTRO_ID" ] || [ -z "$BOARD_DISTRO_VERSION_ID" ]; then
        os_release_tmpfile=$(mktemp)
        if get "/etc/os-release" $os_release_tmpfile ; then
            export BOARD_DISTRO_ID="$(grep ^ID= $os_release_tmpfile | cut -c 4-)"
            export BOARD_DISTRO_VERSION_ID="$(grep ^VERSION_ID= $os_release_tmpfile | cut -c 12-)"
        fi
        rm $os_release_tmpfile
    fi

    if [ -z "$BOARD_DISTRO_ID" ] || [ -z "$BOARD_DISTRO_VERSION_ID" ]; then
            echo "ERROR: could not detect the distribution running on the board."
            echo "Define BOARD_DISTRO_ID and BOARD_DISTRO_VERSION_ID in your board file."
            return 1
    else
        echo "Board distribution is $BOARD_DISTRO_ID $BOARD_DISTRO_VERSION_ID"
    fi
}

# $1 has a single string holding a space-separated args:
#   arg1 = size
#   arg2 = directory to check
# (or optionally) $1 = size, $2 = directory to check
# if no directory is specified, "/" is used as the dir to check
function check_free_storage {
  # here's a neat shell trick to split input on whitespace
  # suppress globbing during processing
  set -f
  arg_array=($1)

  size="${arg_array[0]}"
  dir="${arg_array[1]}"
  set +f

  need_storage_bytes=$(to_bytes "$size")
  echo "need_storage_bytes=${need_storage_bytes}"

  if [ -n "$dir" ] ; then
    storage_dir="$dir"
  else
    if [ -n "$2" ] ; then
      storage_dir="$2"
    else
      storage_dir="/"
    fi
  fi

  have_storage_bytes=$(get_free_storage $storage_dir)
  echo -n "have_storage_bytes=${have_storage_bytes}"
  echo " in storage_dir [${storage_dir}]"

  if [ $have_storage_bytes -gt $need_storage_bytes ] ; then
    echo "OK:    have the needed storage!!"
    return 0
  else
    echo "Error: don't have enough storage"
    return 1
  fi
}

# $1 = a directory to store the full kconfig in
# check /proc/config.gz
# and /boot/config-`uname -r`
# FIXTHIS - get_full_kconfig: if no config.gz, check /lib/modules for configs
#  specifically: /lib/modules/$(uname -r)/kernel/kernel/configs.ko
#  can do 'modprobe configs' to have /proc/config.gz appear on a raspberry pi
function get_full_kconfig {
  out_filename="$1/kconfig"

  gfk_board_tmpfile=$(mktemp)
  gfk_host_tmpfile=$(mktemp)

  if cmd "zcat /proc/config.gz >$gfk_board_tmpfile 2>/dev/null" ; then
    get $gfk_board_tmpfile $out_filename
    return 0
  fi

  kver=$(cmd "uname -r")
  conf_filename="/boot/config-$kver"

  echo "/proc/config.gz not found, trying $conf_filename"

  if cmd "test -f $conf_filename" ; then
    get $conf_filename $out_filename
    return 0
  else
    echo "$conf_filename not found"
  fi

  # try kernel build output directory
  if [ -n "$KBUILD_OUTPUT" ] ; then
    conf_filename="$KBUILD_OUTPUT/.config"
    if [ -e $conf_filename ] ; then
      cp $conf_filename $out_filename
      return 0
    fi
  fi

  # try kernel source directory
  if [ -n "$KERNEL_SRC" ] ; then
    conf_filename="$KERNEL_SRC/.config"
    if [ -e $conf_filename ] ; then
      cp $conf_filename $out_filename
      return 0
    fi
  fi

  # FIXTHIS - check kernel package build area?
  # this depends on the distro and build environment
  return 1
}

# $1 has a single kconfig entry to check
function check_one_kconfig {
  local cfg=$1

  if endswith "$cfg" "=y" ; then
    cfg="^$cfg$"
    result=$(grep -e "$cfg" "$kconf_filename")
    rcode="$?"
    echo "$rcode"
    return $rcode
  fi
  if endswith "$cfg" "=n" ; then
    # see if it's missing, or set to #CONFIG_FOO is not set
    cfg=$(slice $cfg 0 -2)
    result=$(grep $cfg $kconf_filename)
    rcode="$?"
    if [ "$rcode" == "1" ] ; then
      echo "0"
      return 0
    fi
    if endswith "$result" "is not set" ; then
      echo "0"
      return 0
    fi
    # cfg was found, and is set to something
    # but we don't know what
    # FIXTHIS - allow NEED_CONFIG=CONFIG_PRINTK (to be 'y' or 'm')
    # FIXTHIS - allow NEED_CONFIG=CONFIG_SOMETHING=8 (allow ints and strings)
    echo "1"
    return 1
  fi

  # doesn't end in anything special
  # see if cfg ends in [A-Z0-9] (all uppercase)
  if [ $(slice $cfg -1) == "A" ] ; then
    echo "cfg ends in 'A'"
  fi
}

# $1 has single string with a list of kconfig entries to check for
function check_kconfig {
  # split $1 on whitespace, without file globbing
  set -f
  arg_array=($1)
  set +f

  kconf_filename=$LOGDIR/kconfig
  if [ ! -f $kconf_filename ]; then
    get_full_kconfig $LOGDIR
  fi

  for cfg in "${arg_array[@]}" ; do
    echo "Checking cfg: [$cfg]"
    if ! check_one_kconfig $cfg; then
        return 1
    fi
  done
}

# see what effective id is of test user account on board
function check_root {
  euid=$(cmd "id -u")

  if [ "${euid}" = "0" ] ; then
    return 0
  else
    return 1
  fi
}

# check if those specified commands exist on board
# $1 has single string with a list of command entries to check for
function check_program {
  # split $1 on whitespace, without file globbing
  set -f
  arg_array=($1)
  set +f

  for prg in "${arg_array[@]}" ; do
    is_on_target_path $prg prg_path
    if [ -z "$prg_path" ] ; then
      echo -e "\n\nABORTED: Expected command \"$prg\" on the target, but it's not there!"
      return 1
    fi
  done

  # return OK if all necessary commands exist on the target
  return 0
}

# check if those specified module exist on board
# $1 has single string with a list of module entries to check for
function check_module {
  # split $1 on whitespace, without file globbing
  set -f
  arg_array=($1)
  set +f

  for mod in "${arg_array[@]}" ; do
    is_on_target_module $mod mod_path
    if [ -z "$mod_path" ] ; then
      echo -e "\n\nABORTED: Expected module \"$mod\" on the target, but it's not there!"
      return 1
    fi
  done

  # return OK if all necessary modules exist on the target
  return 0
}

function check_needs {
  # check for each possible need variable and process it
  if [ -n "${NEED_MEMORY}" ] ; then
    check_memory "${NEED_MEMORY}"
    rcode=$?
    if [ $rcode -ne 0 ] ; then
      return $rcode
    fi
  fi

  if [ -n "${NEED_FREE_STORAGE}" ] ; then
    check_free_storage "${NEED_FREE_STORAGE}"
    rcode=$?
    if [ $rcode -ne 0 ] ; then
      return $rcode
    fi
  fi

  if [ -n "${NEED_KCONFIG}" ] ; then
    check_kconfig "${NEED_KCONFIG}"
    rcode=$?
    if [ $rcode -ne 0 ] ; then
      return $rcode
    fi
  fi

  if [ -n "${NEED_ROOT}" ] ; then
    check_root
    rcode=$?
    if [ $rcode -ne 0 ] ; then
      echo "Error: Root privileges are required, but not available"
      return $rcode
    fi
  fi

  if [ -n "${NEED_PROGRAM}" ] ; then
    check_program "${NEED_PROGRAM}"
    rcode=$?
    if [ $rcode -ne 0 ] ; then
      return $rcode
    fi
  fi
  if [ -n "${NEED_MODULE}" ] ; then
    check_module "${NEED_MODULE}"
    rcode=$?
    if [ $rcode -ne 0 ] ; then
      return $rcode
    fi
  fi
}

