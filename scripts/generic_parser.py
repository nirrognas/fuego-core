#!/usr/bin/python
#
# generic_parser.py
# This is not really a parser, but it is used as a stub
# results generator when no parser.py is provided by the program.
# This converts the RETURN_VALUE from the test_processing function
# (which usually contains one or more calls to log_compare) into
# the the status for test case: default.<test_name>
#
import os
import sys
import common as plib

RETURN_VALUE=os.environ['RETURN_VALUE']
TESTDIR=os.environ['TESTDIR']

try:
    rcode = int(RETURN_VALUE)
    if rcode == 0:
        value = "PASS"
    else:
        value = "FAIL"
except:
    value = "ERROR"

test_name = TESTDIR.split(".")[1]
test_case_id = "default.%s" % test_name
results = {test_case_id: value}

sys.exit(plib.process(results))
