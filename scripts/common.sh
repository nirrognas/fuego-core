# Copyright (c) 2014 Cogent Embedded, Inc.
# Copyright (c) 2017-2020 Sony Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains common utility functions

# define the default logging levels, and associated output routines
export FUEGO_LL_DEBUG=""
export FUEGO_LL_VERBOSE=""
export FUEGO_LL_INFO=1
export FUEGO_LL_WARNING=1

# set log level based on FUEGO_DEBUG, for backwards compatibility
if [[ -n "$FUEGO_DEBUG" && $(( $FUEGO_DEBUG & 1 )) = 1 ]] ; then
    FUEGO_LL_DEBUG=1
    FUEGO_LL_VERBOSE=1
fi

# set_loglevel - set FUEGO_LL variables depending on FUEGO_LOGLEVELS setting
# $1 has the area
# FUEGO_LOGLEVELS has a string like "build:verbose,run:debug,criteria:warning"
#   this will override the setting of FUEGO_DEBUG, if used
function set_loglevel() {
    set +x
    area="$1"

    # only adjust levels if FUEGO_LOGLEVELS is set
    if [ -n "$FUEGO_LOGLEVELS" ] ; then
	    LL="$FUEGO_LOGLEVELS"
        export FUEGO_LL_DEBUG=""
        export FUEGO_LL_VERBOSE=""
        export FUEGO_LL_INFO=1
        export FUEGO_LL_WARNING=1

        if [[ $LL == *$area* ]] ; then
            if [[ $LL == *$area:debug* ]] ; then
                FUEGO_LL_DEBUG=1
                FUEGO_LL_VERBOSE=1
            fi
            if [[ $LL == *$area:verbose* ]] ; then
                FUEGO_LL_VERBOSE=1
            fi
            if [[ $LL == *$area:warning* ]] ; then
                FUEGO_LL_INFO=""
            fi
            if [[ $LL == *$area:error* ]] ; then
                FUEGO_LL_INFO=""
                FUEGO_LL_WARNING=""
            fi
        fi
    fi

    if [ -n "$FUEGO_LL_DEBUG" ] ; then
        set -x
    fi
}

function dprint() {
    if [ -n "$FUEGO_LL_DEBUG" ] ; then
        echo "DEBUG($FUEGO_CUR_PHASE): $@"
    fi
}

function vprint() {
    if [ -n "$FUEGO_LL_VERBOSE" ] ; then
        echo $@
    fi
}

function iprint() {
    if [ -n "$FUEGO_LL_INFO" ] ; then
        echo $@
    fi
}

function wprint() {
    if [ -n "$FUEGO_LL_WARNING" ] ; then
        echo "### WARNING: $@"
    fi
}

# you can't prevent errors from being printed
function eprint() {
    echo "!!! ERROR: $@"
}

# prepend running python with ORIG_PATH if it exist
function run_python() {
    export RETURN_VALUE=$RETURN_VALUE
    if [ ! -z $ORIG_PATH ] ; then
        dprint "run_python with PATH=$ORIG_PATH, TOOLCHAIN=$TOOLCHAIN"
        export TOOLCHAIN
        PATH=$ORIG_PATH TOOLCHAIN=$TOOLCHAIN python "$@"
    else
        dprint "run_python with TOOLCHAIN=$TOOLCHAIN"
        export TOOLCHAIN
        TOOLCHAIN=$TOOLCHAIN python "$@"
    fi
}

function run_python_quiet() {
    if [ ! -z $ORIG_PATH ]
    then
        PATH=$ORIG_PATH python "$@"
    else
        python "$@"
    fi
}

function abort_job {
# $1 - Abort reason string

  set +x
  echo -e "\n*** ABORTED (during ${FUEGO_CUR_PHASE} phase) ***\n"
  [ -n "$1" ] && echo -e "Fuego error reason: $1\n"

  # also put the abort message to the testlog
  echo -e "\n*** ABORTED (during ${FUEGO_CUR_PHASE} phase) ***\n" >>${LOGDIR}/testlog.txt
  [ -n "$1" ] && echo -e "Fuego error reason: $1\n" >>${LOGDIR}/testlog.txt

  [ -n "$TARGET_TEARDOWN_LINK" ] && $TARGET_TEARDOWN_LINK || true
  ov_transport_disconnect || true

  # BUILD_URL should be defined. But if not, use a default
  if [ -z "$BUILD_URL" ] ; then
    PORT=${JENKINS_PORT:8090}
    BUILD_URL="http://localhost:$PORT/fuego/job/$JOB_NAME/$BUILD_NUMBER"
  fi
  # sync tty data before telling Jenkins to terminate the job
  # so that the abort message will show up in the console log
  sync
  wget -qO- "${BUILD_URL}/stop" > /dev/null
  sleep 15
  echo -e "Jenkins didn't stop the job - quit ourselves"

  # note that we don't send a job 'kill' operation to Jenkins
  # we might already be in post_test, but depending on the size
  # of the logs it might take a long time to retrieve them from the
  # target.  Just exit here, and let the signal handlers do their thing.
  exit 1
}

# check is variable is set and fail if otherwise
# $1 = variable to check
# $2 = optional message if variable is missing
function assert_define () {
    varname=$1
    if [ -z "${!varname}" ]
    then
        if [ -n "$2" ]  ; then
            msg="$2"
        else
            msg="Make sure you use the correct overlays and specs for this test/benchmark."
        fi
        abort_job "$1 is not defined. $msg"
    fi
}

#### here are some handy string functions
# startswith - check if $1 starts with $2
function startswith {
    if [[ "${1:0:${#2}}" = "$2" ]]  ; then
        return 0
    fi
    return 1
}

# endswith - check if $1 ends with $2
function endswith {
	local len2=${#2}
	# check string length first
	if [[ ${#1} -lt $len2 ]] ; then
		return 1
	fi
	start=$(( ${#1} - $len2 ))
	if [ "${1:$start:$len2}" == "$2" ] ; then
		return 0
	fi
	return 1
}

# slice - returns slice of $1, from $2 to $3
# accepts negative index to mean from end of string
function slice {
	local start=$2
	local end=$3

	len1=${#1}
	if [[ $start -lt 0 ]] ; then
		start=$(( $len1 + $start ))
		if [ $start -lt 0 ] ; then
			start=0
		fi
	fi

	if [[ $end -lt 0 ]] ; then
		end=$(( $len1 + $end ))
		if [[ $end -lt 0 ]] ; then
			end=0
		fi
	fi

	count=$(( $end - $start))
	if [[ $count -lt 0 ]] ; then
		count=0
	fi

	echo "${1:$start:$count}"
	return 0
}

# trim - returns $1 with leading and trailing whitespace removed
function trim {
    [[ "$1" =~ [^[:space:]](.*[^[:space:]])? ]]
    printf "%s" "$BASH_REMATCH"
}


# returns 0 if $1 is a version string that is "less than" $2
# e.g. 4.12.15 < 4.12.14
function version_lt() {
    if [ "$1" == "$2" ] ; then
        return 1
    fi
    [ "$1" == "$(echo -e "$1\n$2" | sort -V | head -n1)" ]
}

# support backwards-compatibility with old *-tools.sh files
if [ -z "$TOOLCHAIN" ] ; then
    TOOLCHAIN="$PLATFORM"
fi

if [ -z "$TOOLCHAIN" ] ; then
    TOOLCHAIN="$(ftc query-board -b $NODE_NAME -n TOOLCHAIN)"
fi

if [ -z "$TEST_HOME" ] ; then
    TEST_HOME="${FUEGO_CORE}/tests/${TESTDIR}"
fi
export JOB_BUILD_DIR="${JOB_NAME}-${TOOLCHAIN}"
TEST_BUILD_DIR="${TESTDIR}-${TOOLCHAIN}"

# extract the version and fuego_release from the test.yaml file
# use 0.1-1 as the default if there's some problem
if [ -f "$TEST_HOME/test.yaml" ] ; then
    line=$(grep ^version: $TEST_HOME/test.yaml 2>/dev/null || echo "version: 0.1")
    export TEST_VERSION=$(echo $line | sed "s/version: //")
    line=$(grep ^fuego_release: $TEST_HOME/test.yaml 2>/dev/null || echo "fuego_release: 1")
    export TEST_FUEGO_RELEASE=$(echo $line | sed "s/fuego_release: //")
else
    wprint "Missing test.yaml file - using default version of 0.1-1"
    export TEST_VERSION="0.1"
    export TEST_FUEGO_RELEASE="1"
fi

# FIXTHIS - Fuego versions should be detected at run time
# should use 'git describe' here
export FUEGO_VERSION="v1.5.8"
export FUEGO_CORE_VERSION="v1.5.8"

# use a failsafe default in case it doesn't get set for some reason
export TESTSUITE_VERSION="unknown"

assert_define "FUEGO_CORE"
assert_define "FUEGO_RO"
assert_define "FUEGO_RW"

if [ -z "$FUEGO_HOST" ] ; then
    # FIXTHIS - if the user started this test with a non-standard config
    # path, then this might yield the wrong value.  We don't specify a
    # config filepath here.
    FUEGO_HOST=$(ftc config host_name)
fi
export FUEGO_HOST

if [ -z "${BUILD_TIMESTAMP}" ] ; then
  export BUILD_TIMESTAMP=$(date +%FT%T%z)
fi
if [ -z "${FUEGO_START_TIME}" ] ; then
  start_arr=( $(date +"%s %N") )
  # save start time in milliseconds since the epoch
  # (convert nanoseconds returned by 'date' command to milliseconds)
  export FUEGO_START_TIME="${start_arr[0]}${start_arr[1]:0:3}"
fi

# store some data in a special file, for use by caller
if [ -n "${FUEGO_SPECIAL_DATA_FILE}" ] ; then
  echo "BUILD_NUMBER=$BUILD_NUMBER" >$FUEGO_SPECIAL_DATA_FILE
  echo "TESTSPEC=$TESTSPEC" >>$FUEGO_SPECIAL_DATA_FILE
fi
