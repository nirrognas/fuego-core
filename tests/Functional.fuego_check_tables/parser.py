#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys, collections
import common as plib

measurements = {}
measurements = collections.OrderedDict()

# FIXTHIS - could do better TAP processing here
regex_string = '^(ok|not ok) (\d+) (.*)$'
matches = plib.parse_log(regex_string)

#print("TRB: matches = %s" % matches)

if matches:
    for m in matches:
        parts = m[2].split()
        test_set = parts[0]
        testcase = parts[1]
        measurements[test_set +'.' + testcase] = 'PASS' if m[0] == 'ok' else 'FAIL'

# split the output for each testcase
plib.split_output_per_testcase(regex_string, measurements, False)
## TRB: change last argument to True to test info_follows_regex parsing
# for the per-testcase logs

sys.exit(plib.process(measurements))
