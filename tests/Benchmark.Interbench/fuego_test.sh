tarball=interbench-0.31.tar.bz2

function test_pre_check {
    assert_define BENCHMARK_INTERBENCH_MOUNT_POINT "ERROR: No mount point specified."
    assert_define BENCHMARK_INTERBENCH_MOUNT_BLOCKDEV "ERROR: No block device specified."
}

function test_build {
    patch -p0 < $TEST_HOME/interbench.c.patch
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP"
}

function test_deploy {
    put interbench  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    hd_test_mount_prepare $BENCHMARK_INTERBENCH_MOUNT_BLOCKDEV $BENCHMARK_INTERBENCH_MOUNT_POINT

    report "cd $BENCHMARK_INTERBENCH_MOUNT_POINT; $BOARD_TESTDIR/fuego.$TESTDIR/interbench -L 1 || $BOARD_TESTDIR/fuego.$TESTDIR/interbench -L 1"

    hd_test_clean_umount $BENCHMARK_INTERBENCH_MOUNT_BLOCKDEV $BENCHMARK_INTERBENCH_MOUNT_POINT
}

function test_cleanup {
    kill_procs interbench
}
