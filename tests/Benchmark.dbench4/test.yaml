fuego_package_version: 1
name: Benchmark.dbench4
description: |
    Measure disk throughput for simulated netbench run.
license: GPL-3.0
author: Andrew Tridgell, Ronnie Sahlberg
maintainer: Daniel Sangorrin <daniel.sangorrin@toshiba.co.jp>
version: 4.00
fuego_release: 1
type: Benchmark
tags: ['disk', 'storage', 'performance']
tarball_src:
    - dbench-4.00.tar.gz: https://github.com/sahlberg/dbench (commit: 53ed08d)
    - zlib-1.2.11.tar.gz: http://zlib.net/zlib-1.2.11.tar.gz
gitrepo: https://github.com/sahlberg/dbench
params:
    - MOUNT_BLOCKDEV:
        description: Storage device to be tested.
        example: /dev/sdb1
        optional: no
    - MOUNT_POINT:
        description: Mount point for the storage device
        example: /mnt/
        optional: no
    - TIMELIMIT:
        description: Test duration in seconds including warmup time.
        example: 30
        optional: no
    - NPROCS:
        description: Number of parallel threads.
        example: 2
        optional: no
    - LOADFILE:
        description: |
            One of the loadfiles under the loadfiles/ folder. If an absolute
            path is specified it will be interpreted as a file in the target
            filesystem to be used as load file.
        example: client.txt, nfs.txt, scsi.txt, /usr/share/client.txt
        default: client.txt
        optional: yes
    - BACKEND:
        description: the dbench backend
        example: fileio, sockio, nfs, scsi, iscsi, smb
        optional: yes
    - EXTRAPARAMS:
        description: |
                Additional parameters such as
                -T, --tcp-options=STRING      TCP socket options
                -R, --target-rate=DOUBLE      target throughput (MB/sec)
                -s, --sync                    use O_SYNC
                -S, --sync-dir                sync directory changes
                -F, --fsync                   fsync on write
                -x, --xattr                   use xattrs
                --no-resolve                  disable name resolution simulation
                --clients-per-process=INT     number of clients per process
                --one-byte-write-fix          try to fix 1 byte writes
                --stat-check                  check for pointless calls with stat
                --fake-io                     fake up read/write calls
                --skip-cleanup                skip cleanup operations
                --per-client-results          show results per client
        example: -s -S -F
        optional: yes
data_files:
    - chart_config.json
    - criteria.json
    - dbench-4.00.tar.gz
    - fuego_test.sh
    - parser.py
    - reference.json
    - spec.json
    - test.yaml
