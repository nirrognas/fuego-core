#!/bin/sh
# PDX-License-Identifier: MIT
#
# check_sdhi.sh - unbind and bind sd/mmc device, then use dd command to write random data in the device
#
# Usage: check_sdhi.sh <dev_name> <block_num> <driver_name>
#
# Output is in TAP13 format
#
# Author: Qiu Tingting<qiutt (at) cn.fujitsu.com>
#

usage() {
    cat <<HERE
Usage: check_sdhi.sh [-h] <dev_name> <block_num> <driver_name>

Arguments:
 -h  = show this usage help

dev_name is the name of sd/mmc device in /dev/disk/by-path/
(eg:sh_mobile_sdhi)
block_num is the block of sd/mmc device used for test
(eg:1)
driver_name is the name of sd/mmc driver in /sys/bus/platform/drivers/
(eg:sh_mobile_sdhi)
HERE
   exit 0
}

# parse arguments
if [ "$#" -ne 3 ] ; then
    usage
fi

DEV_NAME="$1"
BLOCK_NUM="$2"
DRIVER_NAME="$3"

echo "TAP version 13"

echo "Test for sd/mmc device unbind/bind and dd write"
echo "DEV_NAME=\"$DEV_NAME\""
echo "BLOCK_NUM=\"$BLOCK_NUM\""
echo "DRIVER_NAME=\"$DRIVER_NAME\""

# set sdhi driver path
sysfs_base_dir="/sys/bus/platform/drivers/$DRIVER_NAME"
sysfs_dev_dir=`find $sysfs_base_dir -name "*$DEV_NAME*"`

if [ ! -e "$sysfs_dev_dir" ] ; then
    echo "there is no $DEV_NAME in path $sysfs_base_dir"
    exit 1
fi

tap_id=1
desc="${tap_id} Test unbind device"
#unbind sd/mmc device
dev_name_for_test=`basename $sysfs_dev_dir`
echo "device name for test $dev_name_for_test"
echo "$dev_name_for_test" > "$sysfs_base_dir/unbind"
if [ -e "$sysfs_dev_dir" ] ; then
    echo "not ok ${desc}"
else
    echo "ok ${desc}"
fi

tap_id=$(( $tap_id + 1 ))
desc="${tap_id} Test bind device"
#bind sd/mmc device
echo "$dev_name_for_test" > "$sysfs_base_dir/bind"
#sleep 30s, wait for device files to be recreated
sleep 30
if [ ! -e "$sysfs_dev_dir" ] ; then
    echo "not ok ${desc}"
    exit 1
else
    echo "ok ${desc}"
fi

write_path="/dev/disk/by-path/"
write_dev=`find $write_path -name "*$DEV_NAME*$BLOCK_NUM"`
echo "write device is $write_dev"

# dd_test uses dd command to write data to sd/mmc device
# $1 the block size for dd command
# $2 the write count for dd command
dd_test ()
{
    bs_size="$1"
    bs_count="$2"

    tap_id=$(( $tap_id + 1 ))
    desc="${tap_id} Test write $bs_size*$bs_count random data"
    IN=$(mktemp)
    OUT=$(mktemp)
    echo "  Write random data to test file"
    dd if=/dev/urandom of="$IN" bs="${bs_size}" count="${bs_count}"

    echo "  Write test data to device"
    dd if="$IN" of="$write_dev" oflag=direct bs="${bs_size}" count="${bs_count}"

    echo "  Read test data from device"
    dd if="$write_dev" of="$OUT" bs="${bs_size}" count="${bs_count}"

    IN_SUM=$(sha256sum "$IN" | cut -f 1 -d ' ')
    OUT_SUM=$(sha256sum "$OUT" | cut -f 1 -d ' ')

    echo "  Compare data writen to data read"
    if [ "$IN_SUM" != "$OUT_SUM" ]; then
        echo "not ok ${desc}"
        IN_SIZE=$(wc -c "$IN" | cut -f 1 -d ' ')
        OUT_SIZE=$(wc -c "$OUT" | cut -f 1 -d ' ')
        echo "  Data read does not match data written"
        echo "    Size (bytes):"
        echo "      in:  $IN_SIZE"
        echo "      out: $OUT_SIZE"
        echo "  SHA 256 Checksums:"
        echo "      in:  $IN_SUM"
        echo "      out: $OUT_SUM"
    else
        echo "ok ${desc}"
    fi
}

# Test write 10M data to sd/mmc
dd_test "1M" "10"

# Test write 1k data to sd/mmc
dd_test "1k" "1"

echo "1..$tap_id"
