tarball=boost.tar.gz

BOOST_LIBRARIES=('libboost_atomic.so' 'libboost_chrono.so' 'libboost_date_time.so')

function test_pre_check {
    for lib in ${BOOST_LIBRARIES[@]}; do
        LIB_VAR_NAME="TARGET_LIB_$(echo ${lib%.*} | tr 'a-z' 'A-Z' | cut -b4-)"
        is_on_target $lib $LIB_VAR_NAME /lib:/usr/lib:/usr/local/lib:/usr/lib/$ARCH-linux-*/:/usr/lib/$TOOLCHAIN-linux-*/
        assert_define $LIB_VAR_NAME
    done
}

function test_build {
    $CXX boost-chrono.c -lboost_system -lboost_chrono -o boost-chrono
    $CXX boost-timer.c -o boost-timer
    $CXX boost-date-time.c -o boost-date-time
    $CXX boost-random.c -o boost-random
    $CXX boost-thread.c -lboost_system -lboost_thread -o boost-thread
    $CXX boost-graph.c -o boost-graph
    $CXX boost-filesystem.c -lboost_filesystem -o boost-filesystem
    $CXX boost-iostream.c -lboost_iostreams -o boost-iostream
    $CXX boost-program-options.c -lboost_program_options -o boost-program-option
    $CXX boost-regex.c -lboost_regex -o boost-regex
    $CXX boost-signal.c -o boost-signal
    $CXX boost-atomic.c -o boost-atomic
    $CXX boost-serialization.c -lboost_serialization  -o boost-serialization
    $CXX boost-system.c -lboost_system -lpthread -o boost-system
    $CXX boost-test.c -o boost-test
    $CXX -g -std=c++11 boost-log.c -o boost-log -rdynamic -lpthread -lboost_log -lboost_system -lboost_thread -lboost_filesystem
}

function test_deploy {
	put boost-chrono $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-timer $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-date-time $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-random $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-thread $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-graph $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-filesystem $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-iostream $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-program-option $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-regex $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-signal $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-atomic $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-serialization $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-system $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-test $BOARD_TESTDIR/fuego.$TESTDIR/
	put boost-log $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *;
	if boost-chrono; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
	if boost-timer; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;\
	if boost-date-time; then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;\
	if boost-random; then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi;\
	if boost-thread; then echo 'TEST-5 OK'; else echo 'TEST-5 FAILED'; fi;\
	if boost-graph; then echo 'TEST-6 OK'; else echo 'TEST-6 FAILED'; fi;\
	if boost-filesystem; then echo 'TEST-7 OK'; else echo 'TEST-7 FAILED'; fi;\
	if boost-iostream; then echo 'TEST-8 OK'; else echo 'TEST-8 FAILED'; fi;\
	if boost-program-option; then echo 'TEST-9 OK'; else echo 'TEST-9 FAILED'; fi;\
	if boost-regex; then echo 'TEST-10 OK'; else echo 'TEST-10 FAILED'; fi;\
	if boost-signal; then echo 'TEST-11 OK'; else echo 'TEST-11 FAILED'; fi;\
	if boost-atomic; then echo 'TEST-12 OK'; else echo 'TEST-12 FAILED'; fi;\
	if boost-serialization; then echo 'TEST-13 OK'; else echo 'TEST-13 FAILED'; fi;\
	if boost-system; then echo 'TEST-14 OK'; else echo 'TEST-14 FAILED'; fi;\
	if boost-test; then echo 'TEST-15 OK'; else echo 'TEST-15 FAILED'; fi;
	if boost-log; then echo 'TEST-16 OK'; else echo 'TEST-16 FAILED'; fi"
}

function test_processing {
	log_compare "$TESTDIR" "16" "^TEST.*OK" "p"
    	log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}




