#!/bin/sh

#  In the target start atd, check the status of job.

test="atrm"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target atd stop

if exec_service_on_target atd start
then
    echo " -> start atd succeeded."
else
    echo " -> start atd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

mkdir test_dir
cp data/test_add.sh test_dir/

at -f test_dir/test_add.sh now + 5 minutes

if atrm $(at -l | cut -b 1-2)
then
    echo " -> $test: TEST-PASS"
else
    echo " -> remove the job failed."
    echo " -> $test: TEST-FAIL"
fi

rm -fr test_dir

at -d $(at -l | cut -b 1-2)

exec_service_on_target atd stop
