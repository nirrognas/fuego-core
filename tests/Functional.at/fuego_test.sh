NEED_ROOT=1

function test_pre_check {
    assert_has_program at
    assert_has_program atq
    assert_has_program atrm
    assert_has_program useradd
    assert_has_program userdel
}

function test_deploy {
    put $TEST_HOME/at_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    ./at_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
