#!/bin/sh
test_path=$(pwd)
rm -rf $test_path/fuse/
mkdir -p $test_path/fuse/test_hello && \
    tar -jxvf fuse_test_libs.tar.bz2 && \
    cd example/.libs || echo " !!!ERROR no fuse_test_libs.tar.bz2 "

if ./hello $test_path/fuse/test_hello ; then
    echo 'TEST-1 OK'; else echo 'TEST-1 FAIL';
fi;
if cat $test_path/fuse/test_hello/hello | grep "Hello World!"; then
    echo 'TEST-2 OK'; else echo 'TEST-2 FAIL';
fi;
if ./fusermount -u $test_path/fuse/test_hello; then
    echo 'TEST-3 OK'; else echo 'TEST-3 FAIL';
fi;

mkdir -p $test_path/fuse/test_fioc
if ./fioc -o allow_other -o sync_read -o nonempty \
    -o intr -o big_writes -o remember=1 -o kernel_cache \
    -o kernel_cache $test_path/fuse/test_fioc; then
    echo 'TEST-4 OK'; else echo 'TEST-4 FAIL';
fi;
if ./fusermount -u $test_path/fuse/test_fioc/; then
    echo 'TEST-5 OK'; else echo 'TEST-5 FALL';
fi;

mkdir -p $test_path/fuse/test_fsel
if ./fsel -o allow_other -o sync_read -o nonempty \
    -o intr -o big_writes -o remember=1 -o kernel_cache \
    -o kernel_cache $test_path/fuse/test_fsel; then
    echo 'TEST-6 OK'; else echo 'TEST-6 FAIL';
fi;
if ./fusermount -u $test_path/fuse/test_fsel/; then
    echo 'TEST-7 OK'; else echo 'TEST-7 FALL';
fi;

# disable the fusexmp test, until it is verified that
# the unmount works.  Otherwise Fuego normal cleanup
# (consisting of rm -rf of $BOARD_TESTDIR) can wipe out
# the root filesystem of a board.
# TRB - 2018-07-31

#mkdir -p $test_path/fuse/test_fusexmp
#if ./fusexmp -o allow_other -o sync_read -o nonempty \
#    -o max_read=16 -o default_permissions \
#    $test_path/fuse/test_fusexmp; then
#    echo 'TEST-8 OK'; else echo 'TEST-8 FAIL';
#fi;
#if ./fusermount -u $test_path/fuse/test_fusexmp/; then
#    echo 'TEST-9 OK'; else echo 'TEST-9 FALL';
#fi;

# there used to be an 'rm -rf $test_path/fuse' here, but due
# to the use of fusexmp and a failure to unmount it (TEST-9),
# the test wiped out 2 of my boards' root filesystems.  Ugh.
