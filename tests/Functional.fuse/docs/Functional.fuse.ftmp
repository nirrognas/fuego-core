===========
Description
===========
Functional.fuse is a test of user-space filesystem support (fuse) and
specifically of the libfuse project.  The libfuse library allows
a program to be written, which implements a user-space filesystem.
The library wraps the interactions between the kernel and the program,
such that the user-space filesystem author only needs to write
functions to be called by the kernel (via the library), to implement
the desired filesystem.

Fuego builds the library, and also a utility program (fusermount) and
some example programs.

The Fuego tests runs a few of the libfuse example programs, including
hello, fioc, fsel and fusexmp.  These example filesystem do the following

* hello - provides a single file 'hello', with the string 'Hello World!\n'
* fioc - provides a single file 'fioc', that can be read and written to,
 and that supports the ioctls FIOC_GET_SIZE and FIOC_SET_SIZE on the file.
  * there is an fioclient used to test this, but it is not currently used by Fuego
* fsel - provides multiple files (0-F) which support the select system calls
  * there is an fselclient used to test this, but it is not currently used by Fuego
* fusexmp - mirrors the root filesystem in the mounted filesystem 

====
Tags
====

* kernel, filesystem

=========
Resources
=========
* `Project source on github <https://github.com/libfuse/libfuse`_

=======
Results
=======

.. fuego_result_list::

========
Variants
========

.. fuego_variant_list::

Variables
---------

This test uses no test-specific variables.


============
Dependencies
============
This test has no dependencies for either the build, or the running system,
in the current configuration used by Fuego.

======
Status
======

.. fuego_status::

=====
Notes
=====
None.
