# this function sleeps for the time specified by the
# spec (default=300 seconds, or 5 minutes)

function test_pre_check {
    assert_define FUNCTIONAL_FUEGO_SLEEP_TIME
}

function test_run {
    ST=$FUNCTIONAL_FUEGO_SLEEP_TIME
    report "echo starting sleeps for $ST seconds"

    SERIES=$(seq --separator=" " $ST)

    # sleep for the total time, in intervals
    report "for i in $SERIES ; do \
                echo -n "." ; \
                sleep 1 ; \
                if [ \$(( \$i % 5 )) = "0" ] ; then \
                    echo ; \
                    echo \"slept for \$i seconds, so far\" ; \
                fi ; \
            done ; \
            echo ; \
            echo DONE!"
}

function test_processing {
    log_compare "$TESTDIR" "1" "DONE" "p"
}
