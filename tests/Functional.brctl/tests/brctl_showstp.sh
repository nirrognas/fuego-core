#!/bin/sh

#  In the target to execute command brct; and confirm the result.
#  option : showstp

test="brctl->showstp"

brctl addbr br0
if brctl showstp br0 | grep "bridge id"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
brctl delbr br0
