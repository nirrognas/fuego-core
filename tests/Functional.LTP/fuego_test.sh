# gitrepo="https://github.com/linux-test-project/ltp.git"
tarball=ltp-full-20180118.tar.bz2

NEED_ROOT=1

ALLTESTS="
admin_tools         fs_perms_simple       ltp-aio-stress.part2  net_stress.interface              quickhit
can                 fs_readonly           ltplite               net_stress.ipsec_dccp             sched
cap_bounds          fsx                   lvm.part1             net_stress.ipsec_icmp             scsi_debug.part1
commands            hugetlb               lvm.part2             net_stress.ipsec_sctp             securebits
connectors          hyperthreading        math                  net_stress.ipsec_tcp              smack
containers          ima                   mm                    net_stress.ipsec_udp              stress.part1
controllers         input                 modules               net_stress.multicast              stress.part2
cpuhotplug          io                    net.features          net_stress.route                  stress.part3
crashme             io_cd                 net.ipv6              net.tcp_cmds                      syscalls
cve                 io_floppy             net.ipv6_lib          net.tirpc_tests                   syscalls-ipc
dio                 ipc                   net.multicast         network_commands                  timers
dma_thread_diotest  kernel_misc           net.nfs               nptl                              tpm_tools
fcntl-locktests     ltp-aiodio.part1      net.rpc               numa                              tracing
filecaps            ltp-aiodio.part2      net.rpc_tests         pipes
fs                  ltp-aiodio.part3      net.sctp              power_management_tests
fs_bind             ltp-aiodio.part4      net_stress.appl       power_management_tests_exclusive
fs_ext4             ltp-aio-stress.part1  net_stress.broken_ip  pty
smoketest"

ALLPTSTESTS="AIO MEM MSG SEM SIG THR TMR TPS"

# This list can be obtained by doing ./testscripts/test_realtime.sh -t list
ALLRTTESTS="
perf/latency
func/measurement
func/hrtimer-prio
func/gtod_latency
func/periodic_cpu_load
func/pthread_kill_latency
func/sched_football
func/pi-tests
func/thread_clock
func/rt-migrate
func/matrix_mult
func/prio-preempt
func/prio-wake
func/pi_perf
func/sched_latency
func/async_handler
func/sched_jitter
"

# OK - the logic here is a bit complicated
# We support several different usage scenarios:
#   1: fuego builds, deploys and installs LTP
#   2: fuego runs an existing installation of LTP
#   3: fuego builds and deploys LTP, to a special location
#      - this is a one-time operation
#      - this sets up the system for 2
#      - this is implemented by spec: "install"
#   4: fuego builds LTP, but deploy is left to user (manual)
#      - this creates a tarball that the user can deploy
#      - this is a one-time operation
#      - this set up the system for 2
#      - this is implemented by spec: "make_pkg"

# $1: string with tests to skip separated by spaces
# $2: absolute path to the skipfile (default: ${LOGDIR}/skiplist.txt)
function skip_tests {
    if [ -n "${FUNCTIONAL_LTP_NOAUTOSKIP}" ]; then
        echo "Not skipping tests: ${TESTS[@]}."
        return 0
    fi

    # split $1 on whitespace, without file globbing
    set -f
    local TESTS=($1)
    local SKIPFILE="${2:-${LOGDIR}/skiplist.txt}"
    set +f

    echo "Skipping tests: ${TESTS[@]}."
    for testname in "${TESTS[@]}"; do
        echo "$testname" >> ${SKIPFILE}
    done
}

# $1: command/program to look for
# $2: string with LTP test names separated by spaces
function skip_if_command_unavailable {
    local PROGRAM="$1"
    local TESTS="$2"
    export FOUND=""

    is_on_target ${PROGRAM} FOUND /bin:/usr/bin:/usr/sbin:/usr/local/bin
    if [ -z "${FOUND}" ]; then
        echo "WARNING: ${PROGRAM} is not installed on the target."
        skip_tests "${TESTS}"
    fi
}

# $1: minimum kernel version
# $2: string with LTP test names separated by spaces
# FIXTHIS: do not skip tests by kernel version if the kernel is an LTS kernel
function skip_if_kver_lt {
    local MIN_KVER="$1"
    local TESTS="$2"
    local KVER=$(cmd "uname -r")

    if version_lt "${KVER}" "${MIN_KVER}"; then
        skip_tests "${TESTS}"
    fi
}

# $1: maximum kernel version
# $2: string with LTP test names separated by spaces
function skip_if_kver_gt {
    local MAX_KVER="$1"
    local TESTS="$2"
    local KVER=$(cmd "uname -r")

    if version_lt "${MAX_KVER}" "${KVER}"; then
        skip_tests "${TESTS}"
    fi
}

# $1: a HAVE_xxx option to grep from ltp's include/config.h (after ./configure)
# $2: string with LTP test names separated by spaces
# $3: optional message
# [Note] can only be called after the configure step has finished
function skip_if_config_unavailable {
    local CONFIG="$1"
    local TESTS="$2"
    local MSG="$3"

    if ! grep "#define $CONFIG" ${WORKSPACE}/${JOB_BUILD_DIR}/include/config.h; then
        echo "WARNING: $CONFIG is not available."
        skip_tests "${TESTS}" "${WORKSPACE}/${JOB_BUILD_DIR}/buildskipfile.txt"
        if [ -n "$MSG" ]; then
            echo "$MSG"
        fi
    fi
}

# $1: string with kernel config options separated by spaces
# $2: string with LTP test names separated by spaces
function skip_if_kconfig_differs {
    set -f
    local KCONFIGS=($1)
    set +f
    local TESTS="$2"

    for cfg in ${KCONFIGS[@]}; do
        if ! check_kconfig "$cfg"; then
            skip_tests "${TESTS}"
        fi
    done
}

function test_pre_check {
    if [ -n "${FUNCTIONAL_LTP_HOMEDIR}" ] ; then
        # user or spec has specified a home directory for LTP.
        # check to see if runltp is installed there
        is_on_target runltp PROGRAM_RUNLTP $FUNCTIONAL_LTP_HOMEDIR

        # if we're not doing a build, then not having LTP installed
        # is a problem
        if [[ "$FUNCTIONAL_LTP_PHASES" != *build* ]] ; then
            assert_define PROGRAM_RUNLTP "Expected LTP to be present on board in $FUNCTIONAL_LTP_HOMEDIR, but it's not there!"
        fi
        LTP_DESTDIR=$FUNCTIONAL_LTP_HOMEDIR

        if [ -z "${FUNCTIONAL_LTP_PHASES}" ] ;  then
            # need to include deploy, because some Fuego-specific files
            # must be deployed, even if rest of LTP is already present
            FUNCTIONAL_LTP_PHASES="deploy run"
        fi
    else
        LTP_DESTDIR=$BOARD_TESTDIR/fuego.$TESTDIR

        if [ -z "${FUNCTIONAL_LTP_PHASES}" ] ;  then
            FUNCTIONAL_LTP_PHASES="build deploy run"
        fi
    fi

    assert_define SDKROOT
    assert_define CC
    assert_define AR
    assert_define RANLIB
    #assert_define LDFLAGS
    assert_define FUNCTIONAL_LTP_TESTS

    # FIXTHIS: use regex for selecting tests to skip once merged on LTP upstream
    echo "Tests skipped by default in Fuego for now"
    echo "# skip these test cases" > ${LOGDIR}/skiplist.txt

    # rest of this pre_check function is automatic skip logic
    # allow user to override this with "noautoskip": "true" in the spec file
    if [ -n "${FUNCTIONAL_LTP_NOAUTOSKIP}" ]; then
        return 0
    fi

    skip_tests "su01" # too complicated to setup
    skip_tests "bdflush01" # bdflush was deprecated by pdflush and pdflush removed from the kernel (https://lwn.net/Articles/508212/)
    skip_tests "fcntl06" # Linux kernel doesn't implement R_GETLK/R_SETLK
    skip_tests "fcntl06_64" # Linux kernel doesn't implement R_GETLK/R_SETLK
    skip_tests "munlockall02" # munlockall can be called from user space
    skip_tests "utimensat01" # kernel patch f2b20f6ee842313a changed return value from -EACCESS to EPERM when a file is immutable and update time is NULL or UTIME_NOW

    echo "Tests skipped if the target /tmp folder is mounted on tmpfs"
    target_mounts=$(mktemp)
    if get "/proc/mounts" $target_mounts; then
        if grep "^tmpfs /tmp " $target_mounts; then
            echo "WARNING: /tmp is mounted using tmpfs."
            skip_tests "fallocate04 readahead02"
        fi
    else
        echo "WARNING: could not check if /tmp is mounted using tmpfs"
    fi
    rm -f $target_mounts

    echo "Tests skipped by Linux kernel version"
    # FIXTHIS: grep tst_kvercmp on LTP to check for other tests that require specific versions
    skip_if_kver_gt "2.6.26" "ptrace03" # Only works for <2.6.26, above that the kernel allows to trace init
    skip_if_kver_lt "3.15" "fallocate04" # The test needs Linux 3.15 or newer
    skip_if_kver_lt "4.2" "inotify06" # will loop/crash kernels that dont have commit 8f2f3eb59dff (<4.2)
    skip_if_kver_lt "4.14" "fanotify07" # see Functional.LTP/docs/Functional.LTP.syscalls.fanotify07.ftmp

    echo "Tests skipped depending on the availability of a command on the target"
    # FIXTHIS: only check the necessary ones
    skip_if_command_unavailable bash "rwtest01 rwtest02 rwtest04 rwtest05 iogen01 fs_inod01 fs_di BindMounts"
    skip_if_command_unavailable expect "su01"
    skip_if_command_unavailable at "at_deny01 at_allow01"
    skip_if_command_unavailable cron "cron cron02 cron_deny01 cron_allow01 cron_dirs_checks01"
    skip_if_command_unavailable ar "ar"
    skip_if_command_unavailable ld "ld01"
    skip_if_command_unavailable ldd "ldd01"
    skip_if_command_unavailable nm "nm01"
    skip_if_command_unavailable file "file01"
    skip_if_command_unavailable tar "tar01"
    skip_if_command_unavailable logrotate "logrotate01"
    skip_if_command_unavailable mkfs.ext3 "ext4-nsec-timestamps"
    skip_if_command_unavailable mkfs.ext4 "ext4-nsec-timestamps"
    skip_if_command_unavailable tune2fs "ext4-nsec-timestamps"
    skip_if_command_unavailable mount "ext4-nsec-timestamps"
    skip_if_command_unavailable umount "ext4-nsec-timestamps"
    skip_if_command_unavailable touch "ext4-nsec-timestamps"
    skip_if_command_unavailable quotacheck "quotactl01"

    # some cmds do not exist in m3ulcb
    skip_if_command_unavailable ftp "ftp04" # for network_commands
    skip_if_command_unavailable ftp "ftp" # for net.tcp_cmds
    skip_if_command_unavailable rsh "route4-change-dst route4-change-gw route4-change-if route4-redirect route4-ifdown route4-rmmod route6-change-dst route6-change-gw route6-change-if route6-redirect route6-ifdown route6-rmmod " # for net_stress.route
    skip_if_command_unavailable rsh "rsh" # for net.tcp_cmds
    skip_if_command_unavailable host "host" # for net.tcp_cmds
    skip_if_command_unavailable rcp "rcp" # for net.tcp_cmds

    echo "Tests skipped depending on the configuration of the target kernel"
    skip_if_kconfig_differs "CONFIG_INOTIFY_USER=y" "inotify_init1_01 inotify_init1_02 inotify01 inotify02 inotify03 inotify04 inotify05 inotify06"
    skip_if_kconfig_differs "CONFIG_FANOTIFY=y CONFIG_FANOTIFY_ACCESS_PERMISSIONS=y" "fanotify01 fanotify02 fanotify03 fanotify04 fanotify05 fanotify06"
    skip_if_kconfig_differs "CONFIG_EXT4_FS=y CONFIG_EXT4DEV_COMPAT=y CONFIG_EXT4_FS_XATTR=y CONFIG_EXT4_FS_POSIX_ACL=y CONFIG_EXT4_FS_SECURITY=y" "ext4-nsec-timestamps"
    skip_if_kconfig_differs "CONFIG_CHECKPOINT_RESTORE=y" "kcmp01 kcmp02 kcmp03"
    skip_if_kconfig_differs "CONFIG_SWAP=y" "swapoff01 swapoff02 swapon01 swapon02 swapon03"
    skip_if_kconfig_differs "CONFIG_SGETMASK_SYSCALL=y" "ssetmask01"

    echo "Tests skipped depending on the architecture"
    if [ "$ARCHITECTURE" != "s390" ]; then
        skip_tests "sbrk03" # Only works in 32bit on s390 series system
    fi

    if [ "$ARCHITECTURE" != "powerpc" ]; then
        skip_tests "switch01" # Requires a 64-bit processor that supports little-endian mode,such as POWER6.
    fi

    if [ "$ARCHITECTURE" != "blackfin" ]; then
        skip_tests "ptrace04" # Requires blackfin processor
    fi

    if [ "$ARCHITECTURE" != "mips" ]; then
        skip_tests "cacheflush01" # Available only on MIPS-based systems
    fi

    if [ "$ARCHITECTURE" = "x86_64" ]; then
        skip_tests "chown01_16 chown02_16 chown03_16 chown04_16 chown05_16"
        skip_tests "fchown01_16 fchown02_16 fchown03_16 fchown04_16 fchown05_16"
        skip_tests "getegid01_16 getegid02_16 geteuid01_16 geteuid02_16 getgid01_16 getgid03_16"
        skip_tests "getgroups01_16 getgroups03_16"
        skip_tests "getuid01_16 getuid03_16"
        skip_tests "lchown01_16 lchown02_16 lchown03_16"
        skip_tests "setfsgid01_16 setfsgid02_16 setfsgid03_16"
        skip_tests "setfsuid01_16 setfsuid02_16 setfsuid03_16 setfsuid04_16"
        skip_tests "setgid01_16 setgid02_16 setgid03_16"
        skip_tests "setgroups01_16 setgroups02_16 setgroups03_16 setgroups04_16"
        skip_tests "setregid01_16 setregid02_16 setregid03_16 setregid04_16"
        skip_tests "setresgid01_16 setresgid02_16 setresgid03_16 setresgid04_16"
        skip_tests "setresuid01_16 setresuid02_16 setresuid03_16 setresuid04_16 setresuid05_16"
        skip_tests "setreuid01_16 setreuid02_16 setreuid03_16 setreuid04_16 setreuid05_16 setreuid06_16 setreuid07_16"
        skip_tests "setuid01_16 setuid02_16 setuid03_16 setuid04_16"
        skip_tests "msgrcv08" # does not work on 64 bit
        skip_tests "readdir21" # This system call does not exist on x86-64 (man 2 readdir)
        skip_tests "ssetmask01" # not in entry/syscalls/syscall_64.tbl
        skip_tests "socketcall01" # not in entry/syscalls/syscall_64.tbl (also mentioned on man socketcall)
    fi

    if [ "$ARCHITECTURE" != "i386" ]; then
        skip_tests "modify_ldt01 modify_ldt02 modify_ldt03" # Only work on i386
    fi

    # now process skips from a file
    get_full_kconfig $LOGDIR
    # kconfig filename will be $LOGDIR/kconfig

    # put reason.json file in log directory also
    SKIPFILE=$LOGDIR/skiplist.txt
    echo "Performing bulk dependency checking using $TEST_HOME/test-dependency-list.txt"
    $FUEGO_CORE/scripts/check-dependencies -b $NODE_NAME \
        -r $LOGDIR/reasons.json \
        $LOGDIR/kconfig \
        $TEST_HOME/test-dependency-list.txt \
          >>$SKIPFILE
}

function test_build {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *build* ]] ; then
        echo "Building LTP"

        patch -p1 <$TEST_HOME/0001-tst_test-Change-result-strings-to-use-T-prefix.patch || true

        if [ ! -f runtest/smoketest ] ; then
            patch -p1 <$TEST_HOME/1-2-runtest-smoketest-Add-smoketest.patch || true
        fi

        # Build the LTP tests
        make autotools
        ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS="$LDFLAGS" SYSROOT="${SDKROOT}" \
            --with-open-posix-testsuite --with-realtime-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX \
            --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu

        echo "Tests skipped depending on the results of LTP ./configure (e.g.: SDK requirements)"
        skip_if_config_unavailable "HAVE_XFS_QUOTA" "quotactl02" "Try adding xfslibs-dev to your SDK"
        skip_if_config_unavailable "HAVE_QUOTAV" "quotactl01 quotactl02" "Try adding libc6-dev to your SDK"
        skip_if_config_unavailable "HAVE_NUMA" "get_mempolicy01 mbind01 migrate_pages01 migrate_pages02 move_pages01 move_pages02 move_pages03 move_pages04 move_pages05 move_pages06 move_pages07 move_pages08 move_pages09 move_pages10 move_pages11" "No NUMA support"
        skip_if_config_unavailable "HAVE_MODIFY_LDT" "modify_ldt01 modify_ldt02 modify_ldt03" "Add modify_ldt support"

        # save build results in build.log
        # Typical build error:
        #   undefined reference to `io_submit'
        # Solution: install libaio for your arch
        #   E.g: /fuego-ro/toolchains/install_cross_toolchain.sh armh
        set -o pipefail
        make CC="${CC}" 2>&1 | tee build.log
        set +o pipefail

        make install

        cp --parents testcases/realtime/scripts/setenv.sh target_bin
        cp $TEST_HOME/ltp_target_run.sh target_bin
    else
        echo "Skipping LTP build"
    fi
}

function test_deploy {
    if [ -f "${WORKSPACE}/${JOB_BUILD_DIR}/buildskipfile.txt" ]; then
        echo "Appending tests skipped during the build phase to the skiplist."
        cat "${WORKSPACE}/${JOB_BUILD_DIR}/buildskipfile.txt" >> ${LOGDIR}/skiplist.txt
    fi

    # Add tests to skip if "skiplist" is defined on the spec. The "skiplist"
    # can contain LTP test_case names (e.g.: inotify06) and/or absolute path(s)
    # to skipfiles (text files containing a list of LTP test case names)
    # usually located under /fuego-rw/boards/
    if [ -n "${FUNCTIONAL_LTP_SKIPLIST}" ]; then
        for item in "${FUNCTIONAL_LTP_SKIPLIST}"; do
            if [ -f "$item" ]; then
                cat "$item" >> ${LOGDIR}/skiplist.txt
            else
                echo "$item" >> ${LOGDIR}/skiplist.txt
            fi
        done
    fi

    # Add tests to skip defined by the board file
    if [ -n "${FUNCTIONAL_LTP_BOARD_SKIPLIST}" ]; then
        for item in "${FUNCTIONAL_LTP_BOARD_SKIPLIST}"; do
            if [ -f "$item" ]; then
                cat "$item" >> ${LOGDIR}/skiplist.txt
            else
                echo "$item" >> ${LOGDIR}/skiplist.txt
            fi
        done
    fi

    # make a staging area
    cp -r target_bin ltp
    cp ${LOGDIR}/skiplist.txt ltp

    if [[ "$FUNCTIONAL_LTP_PHASES" == *deploy* ]] && [[ -z "$PROGRAM_RUNLTP" ]] ; then
        echo "Deploying LTP"
        # the syscalls group occupies by far the largest space so remove it if unneeded
        cp_syscalls=$(echo $FUNCTIONAL_LTP_TESTS | awk '{print match($0,"syscalls|quickhit|ltplite|stress.part|smoketest")}')
        if [ $cp_syscalls -eq 0 ]; then
            echo "Removing syscalls binaries"
            awk '/^[^#]/ { print "rm -f ltp/testcases/bin/" $2 }' ltp/runtest/syscalls | sh
        fi
        put ltp/* $LTP_DESTDIR
    else
        echo "Skipping LTP deploy to board"
    fi

    if [[ "$FUNCTIONAL_LTP_PHASES" == *maketar* ]] ; then
        echo "Creating LTP binary tarball"
        tar zcpf ltp.tar.gz ltp/
        mv ltp.tar.gz ${LOGDIR}/
        echo "LTP tar file ltp.tar.gz is available in ${LOGDIR}, for manual deployment"
    fi

    # remove the staging area
    rm -rf ltp

    # pre-installed LTP needs a few fuego-specific items
    if [ -n "$FUNCTIONAL_LTP_HOMEDIR" ] ; then
       cmd "mkdir -p $FUNCTIONAL_LTP_HOMEDIR/testcases/realtime/scripts"
       put testcases/realtime/scripts/setenv.sh $FUNCTIONAL_LTP_HOMEDIR/testcases/realtime/scripts
       put $TEST_HOME/ltp_target_run.sh $FUNCTIONAL_LTP_HOMEDIR
       put ${LOGDIR}/skiplist.txt $FUNCTIONAL_LTP_HOMEDIR
    fi
}

function test_run {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Running LTP"

        # we patch them here because of the different phases
        cmd "sed -i 's/^fork13 fork13 -i 1000000/fork13 fork13 -i 100000/' $LTP_DESTDIR/runtest/syscalls"
        cmd "sed -i 's/^dio30 diotest6 -b 65536 -n 100 -i 100 -o 1024000/dio30 diotest6 -b 65536 -n 5 -i 100 -o 1024000/' $LTP_DESTDIR/runtest/dio"
        cmd "sed -i 's/^msgctl11 msgctl11/msgctl11 msgctl11 -n 5/' $LTP_DESTDIR/runtest/syscalls"

        TESTS=""
        PTSTESTS=""
        RTTESTS=""

        # ltp_target_run.sh invokes the test different ways, depending
        # on the type of the test (regular, posize, or realtime)
        # Separate the test list by type, and pass the tests in different
        # variables
        for a in $FUNCTIONAL_LTP_TESTS; do
            for b in $ALLTESTS; do
                if [ "$a" == "$b" ]; then
                    TESTS+="$a "
                fi
            done

            for b in $ALLPTSTESTS; do
                if [ "$a" == "$b" ]; then
                    PTSTESTS+="$a "
                fi
            done

            for b in $ALLRTTESTS; do
                if [ "$a" == "$b" ]; then
                    RTTESTS+="$a "
                fi
            done
        done

        # Let some of the tests fail, the information will be in the result xlsx file
        report "cd $LTP_DESTDIR; TESTS=\"$TESTS\"; PTSTESTS=\"$PTSTESTS\"; RTTESTS=\"$RTTESTS\"; . ./ltp_target_run.sh"
    else
        echo "Skipping LTP run"

        # for a build-only test, put the build results into the testlog
        # (via roundtrip to board - pretty wasteful, but oh well...)
        put build.log $BOARD_TESTDIR/fuego.$TESTDIR
        report "cat $BOARD_TESTDIR/fuego.$TESTDIR/build.log"

        report_append "echo 'OK - LTP run skipped.'"
    fi
}

function test_fetch_results {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Fetching LTP results"
        rm -rf result/
        get $LTP_DESTDIR/result $LOGDIR
    else
        echo "Skip fetching LTP run results"
    fi
}

function test_processing {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Processing LTP results"
        SAVEDIR=$(pwd)
        cd ${LOGDIR}/result/
        cp $TEST_HOME/ltp_process.py .

        # TRB - 2020-09-15 ltp_process.py doesn't work due to
        # python error:
        #  from openpyxl.style import Border, Borders, Fill, Color, Alignment
        #  ImportError: No module named style
        if [ -n "$ORIG_PATH" ] ; then
            # Use ORIG_PATH, if defined, so that python works properly
            PATH=$ORIG_PATH python ltp_process.py
        else
            python ltp_process.py
        fi

        [ -e results.xlsx ] && cp results.xlsx ${LOGDIR}/results.xlsx
        [ -e rt.log ] && cp rt.log ${LOGDIR}
        cd ${SAVEDIR}
    else
        echo "Processing LTP build log"
        log_compare "$TESTDIR" "1836" "compile PASSED$" "p"
    fi
}
