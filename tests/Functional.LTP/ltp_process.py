#!/usr/bin/python
# -*- coding: UTF-8 -*-
from openpyxl import Workbook
from openpyxl.styles import Border, Side, PatternFill, Color, Alignment
from openpyxl.styles.borders import BORDER_THIN, BORDER_MEDIUM
from openpyxl.styles.fills import FILL_SOLID
from openpyxl.styles.colors import BLUE, WHITE
import os
import os.path
import re
import sys

def split_output_per_testcase (test_category):
    try:
        os.mkdir("%s/outputs" % test_category)
    except OSError:
        pass

    try:
        output_all = open("%s/output.log" % test_category)
    except IOError:
        print '"%s/result.log" cannot be opened.' % test_category
        sys.exit(1)

    lines = output_all.readlines()
    output_all.close()
    for line in lines:
        m = re.compile("^<<<test_start>>>").match(line)
        if m is not None:
            loop_end = 0
            in_loop = 1
            try:
              output_each = open("%s/outputs/tmp.log" % test_category, "w")
            except IOError:
                print '"%s/outputs/tmp.log" cannot be opened.' % test_category
                sys.exit(1)

        m = re.compile("^tag=([^ ]*)").match(line)
        if m is not None:
            test_case = m.group(1)

        m = re.compile("^<<<test_end>>>").match(line)
        if m is not None:
            loop_end = 1

        if in_loop:
            output_each.write("%s" % line)

        if in_loop & loop_end:
            output_each.close()
            os.rename("%s/outputs/tmp.log" % test_category, "%s/outputs/%s.log" % (test_category, test_case))
            in_loop = 0

def read_cmdline (test_category, test_case):
    try:
        output_each = open("%s/outputs/%s.log" % (test_category, test_case))
    except IOError:
        print '"%s/outputs/%s.log"" cannot be opened.' % (test_category, test_case)
        sys.exit(1)

    output = output_each.read()
    output_each.close()

    m = re.compile("^cmdline=(.*)", re.M | re.S).search(output)
    if m is not None:
        result = m.group(1)
    else:
        result = ""

    return result

def read_output (test_category, test_case):
    try:
        output_each = open("%s/outputs/%s.log" % (test_category, test_case))
    except IOError:
        print '"%s/outputs/%s.log"" cannot be opened.' % (test_category, test_case)
        sys.exit(1)

    output = output_each.read()
    output_each.close()

    m = re.compile("<<<test_output>>>\n(.*)\n<<<execution_status>>>", re.M | re.S).search(output)
    if m is not None:
        result = m.group(1)
    else:
        result = ""

    return result


# Open a workbook
book = Workbook()

# Create style setting before the for loop
# This is needed to avoid excel errors (too many font settings)
medium_borders = Border(left=Side(border_style=BORDER_MEDIUM),
                        right=Side(style=BORDER_MEDIUM),
                        top=Side(style=BORDER_MEDIUM),
                        bottom=Side(style=BORDER_MEDIUM))

thin_borders = Border(left=Side(border_style=BORDER_THIN),
                      right=Side(style=BORDER_THIN),
                      top=Side(style=BORDER_THIN),
                      bottom=Side(style=BORDER_THIN))

white = Color(WHITE)
blue = Color(BLUE)

head_fill = PatternFill(patternType=FILL_SOLID, start_color=Color(rgb='F0C0C0C0'), end_color=Color(rgb='F0C0C0C0'))
pass_fill = PatternFill()
fail_fill = PatternFill(patternType=FILL_SOLID, start_color=Color(rgb='FFFF99CC'), end_color=Color(rgb='FFFF99CC'))
brok_fill = PatternFill(patternType=FILL_SOLID, start_color=Color(rgb='FFCC99FF'), end_color=Color(rgb='FFCC99FF'))
warn_fill = PatternFill(patternType=FILL_SOLID, start_color=Color(rgb='FFCCFF99'), end_color=Color(rgb='FF99FFCC'))
info_fill = PatternFill(patternType=FILL_SOLID, start_color=Color(rgb='FFCC99FF'), end_color=Color(rgb='FFCC99FF'))
conf_fill = PatternFill(patternType=FILL_SOLID, start_color=Color(rgb='FF99CCFF'), end_color=Color(rgb='FF99CCFF'))

wrap_alignment = Alignment(wrap_text=True)

# Enter the for loop that proceses each test folder
tests = os.listdir(".")
tests.sort()
for test_category in tests:
    if not os.path.isdir(test_category):
        continue

    split_output_per_testcase(test_category)

    # Check result.log
    try:
        f = open("%s/result.log" % test_category)
    except IOError:
        print '"%s/result.log" cannot be opened.' % test_category
        continue

    # Add a new sheet
    newSheet = book.create_sheet(title=test_category)
    i = 1

    # Write the header
    headers = ["Test Category", "Test Case", "Date", "Result Value",
               "Result Output", "Result Type", "Result", "Content of test case",
               "Reason of failure", "Comment", "Status"]
    for j in range(1, 12):
        newSheet.cell(row = i, column = j).value = headers[j-1]
        newSheet.cell(row = i, column = j).border = medium_borders
        newSheet.cell(row = i, column = j).fill = head_fill
    i += 1

    lines = f.readlines()
    f.close()
    for line in lines:
        m = re.compile("^startup='([^']*)").match(line)
        if m is not None:
            date = m.group(1)

        m = re.compile("^tag=([^ ]*) stime=([^ ]*) dur=([^ ]*) exit=([^ ]*) stat=([^ ]*) core=([^ ]*) cu=([^ ]*) cs=([^ ]*)").match(line)
        if m is not None:
            test_case = m.group(1)
            result = m.group(5)

            errtype = []
            decision = "O"

            if int(result) == 0:
                errtype.append("PASS")
                fill_data = pass_fill

            if int(result) & 32 != 0:
                errtype.append("CONF")
                decision = "X"
                fill_data = conf_fill

            if int(result) & 16 != 0:
                errtype.append("INFO")
                decision = "X"
                fill_data = info_fill

            if int(result) & 4 != 0:
                errtype.append("WARN")
                decision = "X"
                fill_data = warn_fill

            if int(result) & 2 != 0:
                errtype.append("BROK")
                decision = "X"
                fill_data = brok_fill

            if int(result) & 1 != 0:
                errtype.append("FAIL")
                decision = "X"
                fill_data = fail_fill

            if int(result) & 0x100 != 0:
                decision = "X"
                errtype.append("ERRNO")

            if int(result) & 0x200 != 0:
                decision = "X"
                errtype.append("TERRNO")

            if int(result) & 0x300 != 0:
                decision = "X"
                errtype.append("RERRNO")

            # Read output if result is not SUCCESS
            if decision == "X":
                output = read_output(test_category, test_case)
            else:
                output = ""

            descriptions = [test_category, test_case, date, result,
                            output, "|".join(errtype), decision, "",
                            "", "", ""]
            style_fill = [None, None ,None, fill_data,
                          fill_data, fill_data, fill_data, None,
                          None, None, None]
            style_aline = [None, None, None, None,
                           wrap_alignment, None, None, None,
                           None, None, None]

            for j in range(1, 12):
                newSheet.cell(row = i, column = j).value = descriptions[j - 1]
                newSheet.cell(row = i, column = j).border = thin_borders
                if style_fill[j - 1] is not None:
                    newSheet.cell(row = i, column = j).fill = style_fill[j - 1]
                if style_aline[j - 1] is not None:
                    newSheet.cell(row = i, column = j).alignment = style_aline[j - 1]
            i += 1

    success_data = ["", "", "", "",
                    "", "SUCCESS", "=COUNTIF(OFFSET(INDIRECT(ADDRESS(2, COLUMN())), 0, 0, %d, 1),\"O\")" % (i-2), "",
                    "", "", ""]
    for j in range(1, 12):
        newSheet.cell(row = i, column = j).value = success_data[j - 1]
        newSheet.cell(row = i, column = j).border = medium_borders

    failed_data = ["", "", "", "",
                    "", "FAILED", "=COUNTIF(OFFSET(INDIRECT(ADDRESS(2, COLUMN())), 0, 0, %d, 1),\"X\")" % (i-2), "",
                    "", "", ""]
    for j in range(1, 12):
        newSheet.cell(row = i+1, column = j).value = failed_data[j - 1]
        newSheet.cell(row = i+1, column = j).border = medium_borders

    total_data = ["", "", "", "",
                    "", "TOTAL", "=INDIRECT(ADDRESS(%d, COLUMN())) + INDIRECT(ADDRESS(%d, COLUMN()))" % (i, i+1), "",
                    "", "", ""]
    for j in range(1, 12):
        newSheet.cell(row = i+2, column = j).value = total_data[j - 1]
        newSheet.cell(row = i+2, column = j).border = medium_borders

    # Change colum size
    column_dims = [19, 31, 31, 15, 78, 27, 8, 39, 39, 39, 16]
    j = 0
    for key in sorted(newSheet.column_dimensions.keys()):
        newSheet.column_dimensions[key].width = column_dims[j]
        j += 1

# Posix Test Suite processing
last_was_conformance = False
set_pts_format = False
fills = {'UNRESOLVED':brok_fill, 'FAILED':fail_fill, 'PASS':pass_fill, 'UNTESTED':conf_fill, 'UNSUPPORTED':info_fill, 'SIGNALED':fail_fill}

def pts_set_style(ws):
    for r in range(2, ws.max_row):
        ws.cell(row=r, column=2).fill = fills[str(ws.cell(row=r, column=2).value)]
    # adjust column widths
    dims ={}
    for row in ws.rows:
        for cell in row:
            if cell.value:
                dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value) + 2))
    for col, value in dims.items():
        ws.column_dimensions[col].width = value

if os.path.exists('pts.log'):
    # create one sheet per test group and fill the cells with the results
    with open('pts.log') as f:
        # e.g.: conformance/interfaces/aio_fsync/aio_fsync_14-1: execution: PASS
        regc = re.compile(r"^conformance/(.*): execution: (.*)")
        for line in f:
            line = line.rstrip()
            if line.startswith("ltp_target_run: doing test"):
                test_set = line.split()[3]
                if set_pts_format:
                    pts_set_style(ws)
                ws = book.create_sheet(title=test_set)
                ws.append(["Test", "Result", "Log"])
                last_was_conformance = False
                set_pts_format = True
                continue
            m = regc.match(line)
            if m:
                test_case = m.group(1)
                result = m.group(2)
                if result.startswith("PASS"):
                    result = "PASS"
                elif result.startswith("FAILED"):
                    result = "FAILED"
                elif result.startswith("UNTESTED"):
                    result = "UNTESTED"
                elif result.startswith("UNRESOLVED"):
                    result = "UNRESOLVED"
                elif result.startswith("UNSUPPORTED"):
                    result = "UNSUPPORTED"
                elif result.startswith("SIGNALED"):
                    result = "SIGNALED"
                last_was_conformance = True
                ws.append([test_case, result])
                continue
            if last_was_conformance:
                # add log output
                cell = ws.cell(row=ws.max_row, column=3)
                if cell.value:
                    cell.value = str(cell.value) + '\n' + line
                else:
                    cell.value = line
        # last working sheet
        pts_set_style(ws)

if os.path.exists('rt.log'):
    with open('rt.log') as f:
        rt_testcase_regex = "^--- Running testcase (.*)  ---$"
        rt_results_regex = "^\s*Result:\s*(.*)$"
        ws = book.create_sheet(title="RT tests")
        row = 1
        ws.cell(row=row,column=1).value = "Test"
        ws.cell(row=row,column=2).value = "Result"
        ws.cell(row=row,column=3).value = "Log"
        for line in f:
            m = re.match(rt_testcase_regex, line.rstrip())
            if m:
                test_case = m.group(1)
                row = row + 1
                ws.cell(row=row,column=1).value = test_case
            m = re.match(rt_results_regex, line.rstrip())
            if m:
                test_result = m.group(1)
                ws.cell(row=row,column=2).value = test_result
            if ws.cell(row=row,column=3).value:
                ws.cell(row=row,column=3).value = ws.cell(row=row,column=3).value + line
            else:
                ws.cell(row=row,column=3).value = line

# if we have added sheets, remove the default one ("Sheet")
sheets = book.get_sheet_names()
if len(sheets) > 1:
    book.remove_sheet(book.get_sheet_by_name("Sheet"))

# save the results
book.save('results.xlsx')
