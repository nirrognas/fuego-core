#!/bin/sh
# Script that automates the execution of LTP
# Usage (run from the top LTP dir):
# TESTS="syscalls fs"; PTSTESTS="MEM"; RTTESTS="rt-migrate"; .  ltp_target_run.sh

OUTPUT_DIR=${PWD}/result
TMP_DIR=${PWD}/tmp

[ -d ${TMP_DIR} ] && rm -rf ${TMP_DIR}
mkdir -p ${TMP_DIR}

[ -d ${OUTPUT_DIR} ] && rm -rf ${OUTPUT_DIR}
mkdir -p ${OUTPUT_DIR}

echo y | ./IDcheck.sh

echo "ltp_target_run: ${TESTS} | ${PTSTESTS} | ${RTTESTS}"

# FIXTHIS: add -t option for limiting the duration of each test group execution
for i in ${TESTS}; do
    echo "ltp_target_run: doing test $i"
    mkdir -p ${OUTPUT_DIR}/${i}
    ./runltp -C ${OUTPUT_DIR}/${i}/failed.log \
             -l ${OUTPUT_DIR}/${i}/result.log \
             -o ${OUTPUT_DIR}/${i}/output.log \
             -d ${TMP_DIR} \
             -S ./skiplist.txt \
             -f $i > ${OUTPUT_DIR}/${i}/head.log 2>&1
    rm -rf ${TMP_DIR}/*
done

# gather posix results into pts.log
export LOGFILE=${OUTPUT_DIR}/pts.log
# remove any previous posix results
test -e "${LOGFILE}" && rm "${LOGFILE}"
for i in ${PTSTESTS}; do
    echo "ltp_target_run: doing test $i" | tee -a $LOGFILE
    # PTS scripts seem to have some bashims
    /bin/bash ./bin/run-posix-option-group-test.sh $i
done

for i in ${RTTESTS}; do
    echo "ltp_target_run: doing test $i"
    /bin/bash ./testscripts/test_realtime.sh -t $i
done
[ -e testcases/realtime/logs ] && cat testcases/realtime/logs/*.log > ${OUTPUT_DIR}/rt.log

# LTP always success, check the results to see which subtests failed
exit 0
