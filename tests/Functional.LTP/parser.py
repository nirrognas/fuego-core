#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os, os.path, re, sys
import common as plib

SAVEDIR=os.getcwd()
LOGDIR=os.environ["LOGDIR"]

def abort(msg):
    print msg
    os.chdir(SAVEDIR)
    sys.exit(1)

def split_output_per_testcase (test_category):
    '''
        For each test category/group (e.g. syscalls) there is an output.log
        file that contains the output log of each testcase. This function
        splits output.log into per-testcase files
    '''

    # open input
    try:
        output_all = open("%s/output.log" % test_category)
    except IOError:
        abort('"%s/result.log" cannot be opened.' % test_category)

    # prepare for outputs
    out_dir = test_category + "/outputs"
    try:
        os.mkdir(out_dir)
    except OSError:
        pass

    lines = output_all.readlines()
    output_all.close()
    for line in lines:
        m = re.compile("^<<<test_start>>>").match(line)
        if m is not None:
            loop_end = 0
            in_loop = 1
            try:
              output_each = open(out_dir+"/tmp.log", "w")
            except IOError:
                abort('"%s/tmp.log" cannot be opened.' % out_dir)

        m = re.compile("^tag=([^ ]*)").match(line)
        if m is not None:
            test_case = m.group(1)

        m = re.compile("^<<<test_end>>>").match(line)
        if m is not None:
            loop_end = 1

        if in_loop:
            output_each.write("%s" % line)

        if in_loop & loop_end:
            output_each.close()
            os.rename(out_dir+"/tmp.log", out_dir+"/%s.log" % test_case)
            in_loop = 0

def read_output (test_category, test_case):
    '''
        Reads one of the files splitted by split_output_per_testcase
    '''
    case_log = "%s/outputs/%s.log" % (test_category, test_case)
    try:
        output_each = open(case_log)
    except IOError:
        abort('"%s"" cannot be opened.' % (case_log))

    output = output_each.read()
    output_each.close()

    m = re.compile("<<<test_output>>>\n(.*)\n<<<execution_status>>>", re.M | re.S).search(output)
    if m is not None:
        result = m.group(1)
    else:
        result = ""

    return result


# Check for results dir, and cd there
try:
    os.chdir(LOGDIR+"/result")
except:
    print "WARNING: no result directory (probably a build only test)."
    sys.exit(3)

# there are three types of results - regular, posix and realtime
# parse the regular results, first, into test_results

# Loop that proceses each test folder
tests = os.listdir('.')
tests.sort()
test_results = {}
for test_category in tests:
    if not os.path.isdir(test_category):
        continue

    split_output_per_testcase(test_category)

    ## Check result.log
    try:
        f = open("%s/result.log" % test_category)
    except IOError:
        print '"%s/result.log" cannot be opened.' % test_category
        continue

    lines = f.readlines()
    f.close()
    regc = re.compile("^tag=([^ ]*) stime=([^ ]*) dur=([^ ]*) exit=([^ ]*) stat=([^ ]*) core=([^ ]*) cu=([^ ]*) cs=([^ ]*)")
    for line in lines:
        m = regc.match(line)
        if m is not None:
            test_case = m.group(1)
            result = m.group(5)

            errtype = []
            decision = 0 # 0: PASS, 1: FAIL

            if int(result) == 0:
                errtype.append("PASS")

            if int(result) & 32 != 0:
                errtype.append("CONF")
                decision = 0

            if int(result) & 16 != 0:
                errtype.append("INFO")
                decision = 1

            if int(result) & 4 != 0:
                errtype.append("WARN")
                decision = 1

            if int(result) & 2 != 0:
                errtype.append("BROK")
                decision = 1

            if int(result) & 1 != 0:
                errtype.append("FAIL")
                decision = 1

            if int(result) & 0x100 != 0:
                decision = 1
                errtype.append("ERRNO")

            if int(result) & 0x200 != 0:
                decision = 1
                errtype.append("TERRNO")

            if int(result) & 0x300 != 0:
                decision = 1
                errtype.append("RERRNO")

            if decision == 0:
                print "%s:%s passed" % (test_category, test_case)
                status = "PASS"
            else:
                print "%s:%s failed" % (test_category, test_case)
                status = "FAIL"

            # FIXTHIS: show errtype
            # FIXTHIS: add sub-test data
            test_results[test_category + '.' + test_case] = status

            # put test output to console log
            output = read_output(test_category, test_case)
            print output

# now process posix results - from pts.log file
posix_results = {}
pts_logfile=LOGDIR+"/result/pts.log"
lines = []
if os.path.exists(pts_logfile):
    try:
        f = open(pts_logfile)
        lines = f.readlines()
        f.close()
    except IOError:
        print '"%s" cannot be opened.' % pts_logfile

regc = re.compile(r"^conformance/([^/]*)/([^/]*)/([^/]*): execution: (.*)")
for line in lines:
    m = regc.match(line)
    if m:
        test_set = m.group(2)
        test_case = m.group(3)
        result = m.group(4)

        test_id = test_set+"."+test_case
        status = "ERROR"
        if result.startswith("PASS"):
            status = "PASS"
        elif result.startswith("FAIL"):
            status = "FAIL"
        elif result.startswith("UNTESTED"):
            status = "SKIP"
        posix_results[test_id] = status

# hope no posix tests have the same test_ids as regular tests
test_results.update(posix_results)

if os.path.exists('rt.log'):
    rt_results = {}
    with open('rt.log') as f:
        rt_testcase_regex = "^--- Running testcase (.*)  ---$"
        rt_results_regex = "^\s*Result:\s*(.*)$"
        for line in f:
            m = re.match(rt_testcase_regex, line.rstrip())
            if m:
                test_case = m.group(1)
            m = re.match(rt_results_regex, line.rstrip())
            if m:
                test_result = m.group(1)
                test_id = "rt." + test_case
                rt_results[test_id] = test_result
    test_results.update(rt_results)

os.chdir(SAVEDIR)
sys.exit(plib.process(test_results))

# Posix Test Suite processing
#last_was_conformance = False
#set_pts_format = False
#fills = {'UNRESOLVED':brok_fill, 'FAILED':fail_fill, 'PASS':pass_fill, 'UNTESTED':conf_fill, 'UNSUPPORTED':info_fill}

#def pts_set_style(ws):
    #for r in range(1, ws.get_highest_row()):
        #ws.cell(row=r, column=1).style.fill = fills[str(ws.cell(row=r, column=1).value)]
    ## adjust column widths
    #dims ={}
    #for row in ws.rows:
        #for cell in row:
            #if cell.value:
                #dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value) + 2))
    #for col, value in dims.items():
        #ws.column_dimensions[col].width = value

#if os.path.exists('pts.log'):
    ## create one sheet per test group and fill the cells with the results
    #with open('pts.log') as f:
        #for line in f:
            #line = line.rstrip()
            #if not line:
                #continue
            #splitted = line.split(':')
            #if splitted[0] in ['AIO', 'MEM', 'MSG', 'SEM', 'SIG', 'THR', 'TMR', 'TPS']:
                #if set_pts_format:
                    #pts_set_style(ws)
                #ws = book.create_sheet(title=splitted[0])
                #ws.append(["Test", "Result", "Log"])
                #last_was_conformance = False
                #set_pts_format = True
            #elif splitted[0].startswith('conformance'):
                #last_was_conformance = True
                #ws.append([os.path.basename(splitted[0]), splitted[2].lstrip()])
            #else:
                #if last_was_conformance:
                    #cell = ws.cell(row=ws.get_highest_row() - 1, column=2)
                    #if cell.value:
                        #cell.value = str(cell.value) + '\n' + line
                    #else:
                        #cell.value = line



