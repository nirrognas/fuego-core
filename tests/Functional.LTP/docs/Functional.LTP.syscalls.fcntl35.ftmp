===========
Description
===========
Obtained from fcntl35.c DESCRIPTION:

Description:
fcntl(2) manpage states that an unprivileged user could not set the
pipe capacity above the limit in /proc/sys/fs/pipe-max-size.  However,
an unprivileged user could create a pipe whose initial capacity exceeds
the limit.  We add a regression test to check that pipe-max-size caps
the initial allocation for a new pipe for unprivileged users, but not
for privileged users.

This kernel bug has been fixed by:
commit 086e774a57fba4695f14383c0818994c0b31da7c
Author: Michael Kerrisk (man-pages) <mtk.manpages@gmail.com>
Date:   Tue Oct 11 13:53:43 2016 -0700

pipe: cap initial pipe capacity according to pipe-max-size limit

====
Tags
====

* kernel, syscall, pipe, fcntl

=========
Resources
=========

* http://git.emacinc.com/Linux-Kernel/linux-emac/commit/086e774a57fba4695f14383c0818994c0b31da7c?w=1

=======
Results
=======

Before porting commit 086e774a57fba4695f14383c0818994c0b31da7c:
fcntl35.c:98: FAIL: an unprivileged user init the capacity of a pipe to 65536 unexpectedly, expected 4096

After:
fcntl35.c:101: PASS: an unprivileged user init the capacity of a pipe to 4096 successfully

.. fuego_result_list::

======
Status
======

.. fuego_status::

=====
Notes
=====
