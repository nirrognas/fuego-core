#!/usr/bin/python
# See common.py for description of command-line arguments
# This parser parses TAP output

import os, sys, collections
import common as plib

measurements = {}
measurements = collections.OrderedDict()

# FIXTHIS - could do better TAP processing here
regex_string = '^\[\[.*\]\] (ok|not ok) (\d+) - (.*)$'
matches = plib.parse_log(regex_string)

if matches:
    for m in matches:
        test_id = m[1]
        test_set = "default"
        description = m[2].replace(".","_").replace(" ","_")
        testcase = "%02d_" % int(test_id) + description
        measurements[test_set +'.' + testcase] = 'PASS' if m[0] == 'ok' else 'FAIL'

# split the output for each testcase
plib.split_output_per_testcase(regex_string, measurements)

sys.exit(plib.process(measurements))
