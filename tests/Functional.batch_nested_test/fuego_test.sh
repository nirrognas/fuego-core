# test of Fuego functionality for nested batch tests
#
# Notes:
# - each sub-test is a testcase
# - one of the sub-tests is itself a batch test
#

BATCH_TESTPLAN=$(cat <<END_TESTPLAN
{
    "testPlanName": "nested",
    "default_timeout": "6m",
    "default_spec": "default",
    "default_reboot": "false",
    "default_rebuild": "false",
    "default_precleanup": "true",
    "default_postcleanup": "true",
    "tests": [
        { "testName": "Functional.bc" },
        { "testName": "Functional.batch_hello" },
        { "testName": "Functional.bc" }
    ]
}
END_TESTPLAN
)

function test_run {
    export FUEGO_BATCH_ID="nested-$(allocate_next_batch_id)"

    # don't stop on test errors
    set +e
    log_this "echo \"batch_id=$FUEGO_BATCH_ID\""
    run_test Functional.bc
    run_test Functional.batch_hello
    run_test Functional.bc
    set -e
}
