tarball=../rt-tests/rt-tests-v1.1.1.tar.gz

NEED_ROOT=1
TEST_PROGRAM=deadline_test

function test_pre_check {
    assert_define BENCHMARK_DEADLINETEST_PARAMS
    if check_kconfig "CONFIG_RT_GROUP_SCHED=y"; then
        echo "WARNING: CONFIG_RT_GROUP_SCHED enabled in your kernel. Please check the RT"
        echo "settings as following if this test failed with 'Unable to change scheduling policy'."
        echo "- If the user does not need the RT groups functionality, disable CONFIG_RT_GROUP_SCHED and compile the kernel again, or 'sysctl -w kernel.sched_rt_runtime_us=-1'"
        echo "- If the user does want to use RT groups functionality, you may want to assign the ssh daemon service a realtime budget by specifyihg the cpu.rt_runtime_us attribute"
    fi
}

function test_build {
    patch -p1 -N -s < $TEST_HOME/../rt-tests/0001-Add-scheduling-policies-for-old-kernels.patch
    make NUMA=0 ${TEST_PROGRAM}
}

function test_deploy {
    put ${TEST_PROGRAM}  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./${TEST_PROGRAM} $BENCHMARK_DEADLINE_TEST_PARAMS"
}
