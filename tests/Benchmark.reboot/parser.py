#!/usr/bin/python

import os, re, sys
import common as plib

# pattern that matches reference.log
ref_section_pat = "^\[[\w]+.[gle]{2}\]"

# pattern that matches 'reboot' (requires CONFIG_PRINTK_TIME=y)
cur_search_pat = re.compile("\[\s*(.*)\] Freeing")

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
    for item in pat_result:
        cur_dict["time"] = item

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'Kernel boot time (seconds)'))

