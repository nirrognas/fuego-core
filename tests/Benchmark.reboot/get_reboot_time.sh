#!/bin/sh

# Assume that the kernel boot ends when the last
# message containing 'Freeing' appears.
dmesg | grep -E 'Freeing'

