function test_pre_check {
    assert_has_program nfsstat
    assert_has_program nfsiostat
    assert_has_program rpc.nfsd
    assert_has_program rpc.statd
    assert_has_program rpc.mountd
}

function test_deploy {
    put $TEST_HOME/nfs_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    ./nfs_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
