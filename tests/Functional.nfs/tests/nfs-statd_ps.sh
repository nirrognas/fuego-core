#!/bin/sh

#  In the target start nfs-statd, and confirm the process condition
#  using the ps command.

test="nfs-statd_ps"

. ./fuego_board_function_lib.sh

set_init_manager

service_status=$(get_service_status nfs-statd)
exec_service_on_target nfs-statd stop

if exec_service_on_target nfs-statd start
then
    echo " -> start of nfs-statd succeeded."
else
    echo " -> start of nfs-statd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if ps aux | grep "[/]usr/sbin/rpc.statd"
then
    echo " -> rpc.statd process is running."
else
    echo " -> rpc.statd process is not running."
    echo " -> $test: TEST-FAIL"
    if [ "$service_status" = "inactive" ]
    then
        exec_service_on_target nfs-statd stop
    fi
    exit
fi

exec_service_on_target nfs-statd stop

if ps aux | grep "[/]usr/sbin/rpc.statd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target nfs-statd start
fi
