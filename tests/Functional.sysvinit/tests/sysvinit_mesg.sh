#!/bin/sh

#  In target, run command mesg.

test="mesg"

mesg
mesg_status=$(echo $?)

mesg n
if echo $? = 1
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ $mesg_status = 0 ]
then
    mesg y
fi
