#!/bin/sh

#  In target, run command shutdown.

test="shutdown"

if shutdown -k now
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
