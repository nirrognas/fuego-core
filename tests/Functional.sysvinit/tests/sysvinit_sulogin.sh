#!/bin/sh

#  In target, run command sulogin.

test="sulogin"

expect <<-EOF
spawn sulogin
expect ":"
send "$test_passwd\r"
expect {
"#" {
          send_user " -> $test: TEST-PASS\n"
          }
default { send_user " -> $test: TEST-FAIL\n" }  }
send "exit\r"
expect eof
EOF
