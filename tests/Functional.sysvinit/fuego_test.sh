function test_pre_check {
    assert_has_program halt
    assert_has_program last
    assert_has_program mesg
    assert_has_program runlevel
    assert_has_program shutdown
    assert_has_program sulogin
    assert_has_program utmpdump
    assert_has_program wall
    assert_has_program expect
}

function test_deploy {
    put $TEST_HOME/sysvinit_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    export test_passwd=$PASSWORD;\
    ./sysvinit_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
