function test_pre_check {
    is_on_target_path cryptsetup PROGRAM_CRYPTSETUP
    assert_define PROGRAM_CRYPTSETUP "Missing 'cryptsetup' program on target board"
    is_on_target_path expect PROGRAM_EXPECT
    assert_define PROGRAM_EXPECT "Missing 'expect' program on target board"
}

function test_deploy {
    put $TEST_HOME/cryptsetup_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    ./cryptsetup_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
