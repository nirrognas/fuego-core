# test to test hello with different specs and arguments
#
# Notes:
# - each sub-test is a testcase
# - we use TAP output to summarize the results for this test
#

BATCH_TESTPLAN=$(cat <<END_TESTPLAN
{
    "testPlanName": "hello",
    "default_timeout": "6m",
    "default_spec": "default",
    "default_reboot": "false",
    "default_rebuild": "false",
    "default_precleanup": "true",
    "default_postcleanup": "true",
    "tests": [
        { "testName": "Functional.hello_world" },
        { "testName": "Functional.hello_world", "spec": "hello-fail" },
        { "testName": "Functional.hello_world", "spec": "hello-random" }
    ]
}
END_TESTPLAN
)

function test_run {
    export TC_NUM=1
    DEFAULT_TIMEOUT=3m
    export FUEGO_BATCH_ID="hello-$(allocate_next_batch_id)"

    # don't stop on test errors
    set +e
    log_this "echo \"batch_id=$FUEGO_BATCH_ID\""
    TC_NAME="default"
    run_test Functional.hello_world
    TC_NAME="fail"
    run_test Functional.hello_world -s hello-fail
    TC_NAME="random"
    run_test Functional.hello_world -s hello-random
    set -e
}
