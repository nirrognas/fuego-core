# Functional.fuego_compare_reports
#  This test checks to see if the results from tests that have been run in
#  the last day are any different than for a baseline snapshot taken some
#  time in the past.
#
#  The purpose of this test is to do a high-level comparison of the results
#  of multiple tests, to see if anything has changed in the test results.
#  This intent is to run this immediately after a batch run of multiple
#  tests, to see if that batch produced anything different than expected.
#
#  This test compares the output of ftc list-runs and gen-report, for
#  the board in question, for the current time (when the test is executed)
#  and some previous time (when the baseline snapshot was made)
#
# Usage:
#  preparation:
#    Create 2 jobs:
#     $ ftc add-job -b <board> -t Functional.fuego_compare_reports -s default
#     $ ftc add-job -b <board> -t Functional.fuego_compare_reports -s save_baseline
#    Save baseline reports
#      After a batch run that reflects "normal" behavior for a board, run
#      the 'save_baseline' job:
#     $ ftc build-job <board>.save_baseline.Functional.fuego_compare_reports
#      This will create baseline report files in /fuego-rw/boards
# periodic usage:
#     Check that the overall reports still match, by running the default job:
#     $ ftc build-job <board>.default.Functional.fuego_compare_reports
#
# To do for this test:
# - use better method of determining results to include
#   - results can include manually-run tests, which will not be in the baseline
#     expected data
#   - could use batch-id, which manually-started tests should not have
#     - would have to specify batch id for this test
#       - would require auto-detection of batch id of last batch run (ugh)
#     - requires implementing batch id for batch jobs (saved in run.json file?)
# - handle Benchmark differences
# - omit timestamp field from run-failures
# - suppress operations on the board for this test (this is a host-side only "test")
# - allow user to specify tests to check, with test variable (from spec file)

# suppress gathering current board state information
# FIXTHIS - function override from fuego_test.sh didn't work
#override-func ov_rootfs_state() {
#    return 0
#}

function test_pre_check {
    export baseline_file1=$FUEGO_RW/boards/$NODE_NAME.$TESTDIR.baseline-run-failures.txt
    export baseline_file2=$FUEGO_RW/boards/$NODE_NAME.$TESTDIR.baseline-failures.txt
    export save_baseline=$FUNCTIONAL_FUEGO_COMPARE_REPORTS_SAVE_BASELINE

    # don't check for baseline files if we're doing the save operation
    if [ "$save_baseline" = "true" ] ; then
        return 0
    fi

    if [ ! -f $baseline_file1 ] ; then
        echo "Missing baseline results file: $baseline_file1"
        echo "Maybe try running test with 'save_baseline' spec?"
        abort_job "Missing baseline results file"
    fi

    if [ ! -f $baseline_file2 ] ; then
        echo "Missing baseline results file: $baseline_file2"
        echo "Maybe try running test with 'save_baseline' spec?"
        abort_job "Missing baseline results file"
    fi
}

function test_run {
    echo "Generating reports"

    # get the status for the tests run since yesterday
    # FIXTHIS - should retrieve data based on batch_id instead of time??
    ftc list-runs  --where "board=$NODE_NAME,test!=$TESTDIR,start_time>24 hours ago,status!=PASS" \
        | sort -k 3 >$LOGDIR/test-run-failures.txt
    log_this "echo \"Here is the list-run data:\""
    log_this "cat $LOGDIR/test-run-failures.txt"
    echo "--------"

    # only capture Functional failures in report (test=~Functional.*)
    # omit header_fields and fields (timestamp, report_data) which will never match
    ftc gen-report --where "board=$NODE_NAME,test!=$TESTDIR,test=~Functional.*,start_time>24 hours ago,tguid:result!=PASS" \
        --header_fields test,board,kernel \
        --fields test_name,spec,board,tguid,tguid:result \
        >$LOGDIR/test-failures.txt
    log_this "echo \"Here is the gen-report data:\""
    log_this "cat $LOGDIR/test-failures.txt"

    # if we're doing the "save_baseline" spec, save reports
    if [ "$save_baseline" = "true" ] ; then
        cp $LOGDIR/test-run-failures.txt $baseline_file1
        log_this "echo \"baseline file: $baseline_file1 saved\""
        cp $LOGDIR/test-failures.txt $baseline_file2
        log_this "echo \"baseline file: $baseline_file1 saved\""
        log_this "echo baseline save: OVERALL SUCCESS"
        return 0
    fi

    # check for differences from baseline
    set +e
    log_this "echo \"Checking for differences in failed test suites (list-runs)\""
    log_this "diff -u $baseline_file1 $LOGDIR/test-run-failures.txt"
    diff_result1="$?"
    log_this "echo ------------------------"

    log_this "echo \"Checking for differences in the failed testcases (gen-report)\""
    log_this "diff -u $baseline_file2 $LOGDIR/test-failures.txt"
    diff_result2="$?"
    log_this "echo ------------------------"
    set -e

    if [ $diff_result1 == "0" -a $diff_result2 == "0" ] ; then
        log_this "echo \"no changes found between today's report and baseline\""
        log_this "echo \"OVERALL SUCCESS\""
    else
        log_this "echo \"some changes found between today's report and baseline\""
        log_this "echo \"OVERALL FAILURE\""
    fi
}

function test_processing {
    log_compare $TESTDIR 1 "OVERALL SUCCESS" "p"
}
