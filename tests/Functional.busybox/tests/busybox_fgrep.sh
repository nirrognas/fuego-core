#!/bin/sh

#  The testscript checks the following options of the command fgrep
#  1) Option: -i

test="fgrep"

mkdir test_dir
echo "This is a test file." > ./test_dir/test1
echo "It contains data to test the fgrep function." >> ./test_dir/test1
echo "fgrep is the same as grep -F function." >> ./test_dir/test1
echo "It interprets the pattern as fixed string." >> ./test_dir/test1
echo "In the test we shall look for a fixed pattern." >> ./test_dir/test1

busybox fgrep in ./test_dir/test1 > log
if [ "$(head -n 1 log)" = "It contains data to test the fgrep function." ] && [ "$(tail -n 1 log)" = "It interprets the pattern as fixed string." ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir
