#!/bin/sh
workdir=$(pwd)
test="ash"

expect <<-EOF
spawn busybox ash
expect "*# "
send_user " -> $test: Opened ash shell succeeded.\n"
send "busybox pwd\r"
expect "$workdir"
send_user " -> $test: Executed pwd command ash shell succeeded.\n"
send "exit\r"
send_user " -> $test: TEST-PASS\n"
expect eof
EOF
