#!/bin/sh

#  The testscript checks the following options of the command busybox
#  1) Option none

test="busybox"

if busybox | grep ^BusyBox
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
