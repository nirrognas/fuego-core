#!/bin/sh

#  The testscript checks the following options of the command mkdir
#  1) Option none

test="mkdir"

busybox mkdir test_dir
if ls -l | grep test_dir
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rmdir test_dir;
