#!/bin/sh

#  The testscript checks the following options of the command killall
#  1) Option none

test="killall"

secs=17
sleep $secs &
sleep $secs &

if ps | grep [s]leep
then
    echo " -> $test: sleep succeeded."
else
    echo " -> $test: sleep failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

busybox killall sleep
if ps | grep [s]leep
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi;
