#!/bin/sh

#  The testscript checks the following options of the command tee
#  1) Option: none

test="tee"

rm -rf ./test_dir
mkdir ./test_dir
if [ "$(echo "Hello World" | busybox tee ./test_dir/test1)" = "Hello World" ]
then
    echo " -> $test: echo "Hello World" | busybox tee ./test_dir/test1 executed."
else
    echo " -> $test: echo "Hello World" | busybox tee ./test_dir/test1 failed."
    echo " -> $test: TEST-FAIL"
    rm -fr ./test_dir
    exit
fi;

if [ "$(cat ./test_dir/test1)" = "Hello World" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr ./test_dir
