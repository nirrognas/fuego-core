#!/bin/sh

#  The testscript checks the following options of the command sh
#  1) Option none

test="sh"

mkdir test_dir
touch test_dir/test1

cat >> test_dir/test1 <<EOF
#!/bin/bash
echo "this is test bash!"
EOF

if busybox sh test_dir/test1 | grep "this is test bash!"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
