#!/bin/sh

#  The testscript checks the following options of the command ps
#  1) Option none

test="ps"

if busybox ps | grep "[b]usybox ps"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
