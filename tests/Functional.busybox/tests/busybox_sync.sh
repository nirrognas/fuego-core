#!/bin/sh

#  The testscript checks the following options of the command sync
#  1) Option none

test="sync"

if busybox sync
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
