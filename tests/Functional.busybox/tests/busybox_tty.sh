#!/bin/sh

#  The testscript checks the following options of the command tty
#  1) Option: none

test="tty"

if busybox tty
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
