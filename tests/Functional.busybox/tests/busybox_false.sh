#!/bin/sh

#  The testscript checks the following options of the command false
#  1) Option none

test="false"

if busybox false
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi;
