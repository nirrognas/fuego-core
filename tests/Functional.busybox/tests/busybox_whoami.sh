#!/bin/sh

#  The testscript checks the following options of the command whoami
#  1) Option none

test="whoami"

if [ "$(busybox whoami)" = "$(id -un)" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
