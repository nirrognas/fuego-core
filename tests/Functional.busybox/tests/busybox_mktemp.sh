#!/bin/sh

#  The testscript checks the following options of the command mktemp
#  1) Option none

test="mktemp"

mkdir test_dir
if [ "$(busybox mktemp test_dir/test_file.XXXXXX | grep "test_dir\/test_file\.[a-zA-Z0-9]\{6\}")" ]
then
    echo " -> $test: mktemp command succeeded."
else
    echo " -> $test: mktemp command failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir;
    exit
fi;

if ls -l test_dir | grep "\-rw[\-]\{7\}.*test_file\.[a-zA-Z0-9]\{6\}"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir;
