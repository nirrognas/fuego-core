#!/bin/sh

#  In target, run command pam_timestamp_check.
#  option "-k"

test="pam_timestamp_check"

if pam_timestamp_check -k
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
