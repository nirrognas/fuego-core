# test to experiment with a different form of batch jobs
# right now, Fuego uses testplans for batch jobs
# This attempts to run multiple tests as nested tests
# from this batch test.
#
# Notes:
# - each sub-test is a testcase
# - we use TAP output to summarize the results for this test
#

BATCH_TESTPLAN=$(cat <<END_TESTPLAN
{
    "testPlanName": "default",
    "default_timeout": "40m",
    "default_spec": "default",
    "default_reboot" : "false",
    "default_rebuild" : "false",
    "default_precleanup" : "true",
    "default_postcleanup" : "true",
    "tests": [
        { "testName": "Benchmark.Dhrystone"
        },
        { "testName": "Benchmark.dbench4"
        },
        { "testName": "Benchmark.gtkperf"
        },
        { "testName": "Benchmark.GLMark"
        },
        { "testName": "Benchmark.ebizzy"
        },
        { "testName": "Benchmark.Java"
        },
        { "testName": "Benchmark.hackbench"
        },
        { "testName": "Benchmark.himeno"
        },
        { "testName": "Benchmark.Interbench"
        },
        { "testName": "Benchmark.iperf"
        },
        { "testName": "Benchmark.netperf"
        },
        { "testName": "Benchmark.netpipe"
        },
        { "testName": "Benchmark.OpenSSL"
        },
        { "testName": "Benchmark.Stream"
        },
        { "testName": "Benchmark.Whetstone"
        },
        { "testName": "Benchmark.x11perf"
        },
        { "testName": "Benchmark.signaltest"
        },
        { "testName": "Benchmark.linpack"
        },
        { "testName": "Benchmark.nbench_byte"
        },
        { "testName": "Benchmark.cyclictest"
        },
        { "testName": "Functional.bc"
        },
        { "testName": "Benchmark.bonnie"
        },
        { "testName": "Benchmark.ffsb"
        },
        { "testName": "Benchmark.fio"
        },
        { "testName": "Benchmark.IOzone"
        },
        { "testName": "Benchmark.lmbench2"
        },
        { "testName": "Benchmark.tiobench"
        },
        { "testName": "Functional.kernel_build",
           "timeout": "100m"
        },
        { "testName": "Functional.LTP",
          "timeout": "120m"
        },
        { "testName": "Functional.OpenSSL"
        },
        { "testName": "Functional.aiostress"
        },
        { "testName": "Functional.bzip2"
        },
        { "testName": "Functional.crashme"
        },
        { "testName": "Functional.expat"
        },
        { "testName": "Functional.fontconfig"
        },
        { "testName": "Functional.ft2demos"
        },
        { "testName": "Functional.glib"
        },
        { "testName": "Functional.ipv6connect"
        },
        { "testName": "Functional.jpeg"
        },
        { "testName": "Functional.linus_stress"
        },
        { "testName": "Functional.pi_tests"
        },
        { "testName": "Functional.rmaptest"
        },
        { "testName": "Functional.netperf"
        },
        { "testName": "Functional.scrashme"
        },
        { "testName": "Functional.stress"
        },
        { "testName": "Functional.synctest"
        },
        { "testName": "Functional.zlib"
        },
        { "testName": "Functional.hello_world"
        }
    ]
}
END_TESTPLAN
)

function test_run {
    export TC_NUM=1
    DEFAULT_TIMEOUT=3m
    export FUEGO_BATCH_ID="nightly2-$(allocate_next_batch_id)"

    # don't stop on test errors
    set +e
    log_this "echo \"batch_id=$FUEGO_BATCH_ID\""
    run_test Functional.fuego_board_check --timeout 10m
    run_test Benchmark.Dhrystone
    run_test Benchmark.dbench4
    run_test Benchmark.gtkperf
    run_test Benchmark.GLMark
    run_test Benchmark.ebizzy
    run_test Benchmark.Java
    run_test Benchmark.hackbench
    run_test Benchmark.himeno
    run_test Benchmark.Interbench
    run_test Benchmark.iperf
    run_test Benchmark.netperf
    run_test Benchmark.netpipe
    run_test Benchmark.OpenSSL
    run_test Benchmark.Stream
    run_test Benchmark.Whetstone
    run_test Benchmark.x11perf
    run_test Benchmark.signaltest
    run_test Benchmark.linpack
    run_test Benchmark.nbench_byte
    run_test Benchmark.cyclictest
    run_test Functional.bc
    run_test Benchmark.bonnie
    run_test Benchmark.ffsb
    run_test Benchmark.fio
    run_test Benchmark.IOzone
    run_test Benchmark.lmbench2
    run_test Benchmark.tiobench
    run_test Functional.kernel_build --timeout 100m
    run_test Functional.LTP --timeout 120m
    run_test Functional.OpenSSL --reboot true
    run_test Functional.aiostress
    run_test Functional.bzip2
    run_test Functional.crashme
    run_test Functional.expat
    run_test Functional.fontconfig
    run_test Functional.ft2demos
    run_test Functional.glib
    run_test Functional.hello_world
    run_test Functional.ipv6connect
    run_test Functional.jpeg
    run_test Functional.linus_stress
    run_test Functional.pi_tests
    run_test Functional.rmaptest
    run_test Functional.netperf
    run_test Functional.scrashme
    run_test Functional.stress
    run_test Functional.synctest
    run_test Functional.zlib
    set -e
}
