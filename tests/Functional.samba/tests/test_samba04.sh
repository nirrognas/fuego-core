#!/bin/sh

#  Set the server to access the target even if
#  you don't have a password from the server,
#  and make sure the server can access the target.

test="smbd04"

smb_status=$(get_service_status smb)
iptables_status=$(get_service_status iptables)

exec_service_on_target smb stop
exec_service_on_target iptables stop

mkdir -p /home/test/samba
mkdir -p /home/test/samba_test

cp data/test1 /home/test/samba/

chown nobody:nobody -R /home/test/samba

if [ -f /etc/samba/smb.conf ]
then
    mv /etc/samba/smb.conf /etc/samba/smb.conf_bak
fi

restore_target(){
    if [ -f /etc/samba/smb.conf_bak ]
    then
        mv /etc/samba/smb.conf_bak /etc/samba/smb.conf
    fi
    if [ "$iptables_status" = "active" -o "$iptables_status" = "unknown" ]
    then
        exec_service_on_target iptables start
    fi
    if [ "$smb_status" = "inactive" ]
    then
        exec_service_on_target smb stop
    fi
}

sed -i 's/test_target/'"$test_target"'/g' data/smb04.conf
cp data/smb04.conf /etc/samba/smb.conf

if exec_service_on_target smb start
then
    echo " -> start of smb succeeded."
else
    echo " -> start of smb failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 5

expect <<-EOF
spawn smbclient //$test_target/test -N
expect {
 -re ".*smb: .*" {
           send "get test1 /home/test/samba_test/test1\n"
          }
 default { send_user "Can not log into the board.\n"}  }
expect {
 -re ".*smb: .*" {
           send "\n"
          }
 default { send_user "Can not log into the board.\n"}  }
send "exit\n"
expect eof
EOF

if ls /home/test/samba_test/test1
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

restore_target
