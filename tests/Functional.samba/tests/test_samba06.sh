#!/bin/sh

#  Start the service samba and keep it running on the target

. ./fuego_board_function_lib.sh
set_init_manager

echo $(get_service_status smb) > $3
echo $(get_service_status iptables) >> $3

exec_service_on_target smb stop
exec_service_on_target iptables stop

mkdir -p /home/test/samba

cp data/test1 /home/test/samba/

chown nobody:nobody -R /home/test/samba

if [ -f /etc/samba/smb.conf ]
then
    mv /etc/samba/smb.conf /etc/samba/smb.conf_bak
fi

restore_target(){
    if [ -f /etc/samba/smb.conf_bak ]
    then
        mv /etc/samba/smb.conf_bak /etc/samba/smb.conf
    fi
    if [ "$iptables_status" = "active" -o "$iptables_status" = "unknown" ]
    then
        exec_service_on_target iptables start
    fi
    if [ "$smb_status" = "inactive" ]
    then
        exec_service_on_target smb stop
    fi
}

sed -i 's/test_target/'"$2"'/g' data/smb06.conf
sed -i 's/test_host/'"$1"'/g' data/smb06.conf
cp data/smb06.conf /etc/samba/smb.conf

if exec_service_on_target smb start
then
    echo " -> start of smb succeeded."
else
    echo " -> start of smb failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5
