#!/bin/sh

#  In the target start nscd, and confirm the process condition by command ps.

test="ps"

nscd_status=$(get_service_status nscd)

exec_service_on_target nscd stop

if exec_service_on_target nscd start
then
    echo " -> start of nscd succeeded."
else
    echo " -> start of nscd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if ps aux | grep "[/]usr/sbin/nscd"
then
    echo " -> get the process of nscd."
else
    echo " -> can't get the process of nscd."
    echo " -> $test: TEST-FAIL"
    if [ "$nscd_status" = "inactive" ]
    then
        exec_service_on_target nscd stop
    fi
    exit
fi

exec_service_on_target nscd stop

if ps aux | grep "[/]usr/sbin/nscd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

if [ "$nscd_status" = "active" -o "$nscd_status" = "unknown" ]
then
    exec_service_on_target nscd start
fi
