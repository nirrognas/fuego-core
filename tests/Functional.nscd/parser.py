#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys, collections
import common as plib

measurements = {}
measurements = collections.OrderedDict()

regex_string = '^ -> (.*): TEST-(.*)$'
matches = plib.parse_log(regex_string)

if matches:
    for m in matches:
        measurements['default.' + m[0]] = 'PASS' if m[1] == 'PASS' else 'FAIL'

# split the output for each testcase
plib.split_output_per_testcase(regex_string, measurements)

sys.exit(plib.process(measurements))
