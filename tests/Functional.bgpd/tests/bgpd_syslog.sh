#!/bin/sh

#  In the target start bgpd and zebra.
#  At the same time, start syslog-ng and check the keyword "bgpd" in syslog.

test="syslog"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target bgpd stop
exec_service_on_target zebra stop
exec_service_on_target syslog-ng stop

rm -f /var/run/quagga/bgpd.pid
#Backup the config file
mv /etc/quagga/bgpd.conf /etc/quagga/bgpd.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck
#Backup the syslog file
mv /var/log/syslog /var/log/syslog.bck

cp data/bgpd.conf /etc/quagga/bgpd.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target syslog-ng restart
then
    echo " -> restart of syslog-ng succeeded."
else
    echo " -> restart of syslog-ng failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target bgpd start
then
    echo " -> start of bgpd succeeded."
else
    echo " -> start of bgpd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if cat /var/log/syslog | grep "bgpd"
then
    echo " -> get the syslog of bgpd."
    echo " -> $test: TEST-PASS"
else
    echo " -> can't get the syslog of bgpd."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target bgpd stop
exec_service_on_target zebra stop

#Restore the config file
mv /etc/quagga/bgpd.conf.bck /etc/quagga/bgpd.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf
#Restore the syslog file
mv /var/log/syslog.bck /var/log/syslog
