#!/bin/sh

#  In the target start bgpd and zebra, then confirm the process condition by command ps.
#  check the keyword "quagga/bgpd".

test="ps"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target bgpd stop
exec_service_on_target zebra stop

#Backup the config file
mv /etc/quagga/bgpd.conf /etc/quagga/bgpd.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck

cp data/bgpd.conf /etc/quagga/bgpd.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target bgpd start
then
    echo " -> start of bgpd succeeded."
else
    echo " -> start of bgpd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps -N a | grep "quagga/[b]gpd"
then
    echo " -> get the pid of bgpd."
else
    echo " -> can't get the pid of bgpd."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target bgpd stop
exec_service_on_target zebra stop

if ps -N a | grep "quagga/[b]gpd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

#Restore the config file
mv /etc/quagga/bgpd.conf.bck /etc/quagga/bgpd.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf
