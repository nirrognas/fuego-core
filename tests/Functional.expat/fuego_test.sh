tarball=expat-2.0.0.tar.gz

function test_build {
    # This patch adds pass/fail output for tests.
    cd tests
    patch -p0 < $TEST_HOME/xmltest.sh.patch
    patch -p2 < $TEST_HOME/xmltest_use_local_xmlwf.patch
    patch -p2 < $TEST_HOME/int_cast_fix.patch
    cd -

    # XML Test Suite
    mkdir -p XML-Test-Suite

    # Possible defect - hardcoded xmlts name
    tar zxf $TEST_HOME/xmlts20080827.tar.gz -C XML-Test-Suite

    # get updated config.sub and config.guess files, so configure
    # doesn't reject new toolchains
    cp /usr/share/misc/config.{sub,guess} conftools
    CXXFLAGS='-I. -I./lib -g -O2'
    ./configure --build=`uname -m`-gnu-linux --host="$PREFIX" #CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP"
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" || return
    make CFLAGS="$CFLAGS" CC="$CC" CXX=$PREFIX-g++ CXX="$CXX"  CXXFLAGS="$CXXFLAGS" tests/runtestspp
}

function test_deploy {
    tar cf XML-Test-Suite.tar XML-Test-Suite/
    put XML-Test-Suite.tar tests/.libs/* tests/xmltest.sh  $BOARD_TESTDIR/fuego.$TESTDIR/

    cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR; tar xf XML-Test-Suite.tar"
    cmd "mkdir -p $BOARD_TESTDIR/fuego.$TESTDIR/xmlwf"

    put xmlwf/.libs/xmlwf  $BOARD_TESTDIR/fuego.$TESTDIR/xmlwf/xmlwf
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./runtestspp 2>&1 | tr ',' '\n'| sed s/^\ //"
    report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./xmltest.sh"
}

function test_processing {
    assert_define FUNCTIONAL_EXPAT_SUBTEST_COUNT_POS
    assert_define FUNCTIONAL_EXPAT_SUBTEST_COUNT_NEG

    log_compare "$TESTDIR" $FUNCTIONAL_EXPAT_SUBTEST_COUNT_POS "^.*\.xml passed\.$" "p"
    log_compare "$TESTDIR" $FUNCTIONAL_EXPAT_SUBTEST_COUNT_NEG "^.*\.xml failed\.$" "n"
}

