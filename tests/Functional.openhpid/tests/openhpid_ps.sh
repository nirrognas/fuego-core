#!/bin/sh

#  In the target start openhpid, and confirm the process condition by command ps.

test="ps"

. ./fuego_board_function_lib.sh

set_init_manager

service_status=$(get_service_status openhpid)

exec_service_on_target openhpid stop

if exec_service_on_target openhpid start
then
    echo " -> $test: TEST-PASS"
else
    echo " -> start of openhpid failed."
    echo " -> $test: TEST-FAIL"
fi

if [ "$service_status" = "inactive" ]
then
    exec_service_on_target openhpid stop
fi
