gitrepo="https://github.com/Linaro/test-definitions.git"

# Root permissions required for
# - installing dependencies on the target (debian/centos) (unless SKIPINSTALL=1)
# - executing some of the tests
# FIXTHIS: don't force root permissions for tests that do not require them
NEED_ROOT=1

function test_pre_check {
    # linaro parser dependencies
    assert_has_program sed
    assert_has_program awk
    assert_has_program egrep

    # test-runner requires a password-less connection
    # Eg: Inside fuego's docker container do
    # su jenkins
    # cp path/to/bbb_id_rsa ~/.ssh/
    # vi ~/.ssh/config
    #  Host 192.167.1.99 <- replace with your boards ip address ($IPADDR)
    #    IdentityFile ~/.ssh/bbb_id_rsa
    assert_define SSH_KEY "Please setup SSH_KEY on your board file (fuego-ro/boards/$NODE_NAME.board)"
}

function test_build {
    source ./automated/bin/setenv.sh
    pip install -r $REPO_PATH/automated/utils/requirements.txt --user
}

function test_run {
    source $WORKSPACE/$JOB_BUILD_DIR/automated/bin/setenv.sh

    yaml_file=${FUNCTIONAL_LINARO_YAML:-"automated/linux/smoke/smoke.yaml"}
    if [ ! -e "${REPO_PATH}/$yaml_file" ]; then
            abort_job "$yaml_file not found"
    fi

    if startswith "$yaml_file" "plans"; then
            echo "using test plan: $yaml_file"
            test_or_plan_flag="-p"
    else
            echo "using test definition: $yaml_file"
            test_or_plan_flag="-d"
    fi

    if [ -n "$FUNCTIONAL_LINARO_PARAMS" ]; then
        PARAMS="-r $FUNCTIONAL_LINARO_PARAMS"
    else
        PARAMS=""
    fi

    # Note: linaro already detects if the OS supports installing dependencies
    SKIPINSTALL=${FUNCTIONAL_LINARO_SKIPINSTALL:-0}
    if [ "$SKIPINSTALL" -eq 1 ]; then
        SKIPFLAG="-s"
    else
        SKIPFLAG=""
    fi

    test-runner -o ${LOGDIR} $test_or_plan_flag ${REPO_PATH}/$yaml_file $PARAMS -g $LOGIN@$IPADDR $SKIPFLAG -e
}

# FIXTHIS: the log directory is populated with a copy of the whole repository, clean unnecessary files
