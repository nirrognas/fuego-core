#!/bin/sh

# Start the named on target.
# Check the listening port.

test="named_lport"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
exec_service_on_target dnsmasq stop
if [ -f /etc/bind/rndc.key ]
then
    rm -f /etc/bind/rndc.key
fi

exec_service_on_target named stop

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    exit
fi

if netstat -ln | grep ":53"
then
    echo " -> port 53 is exist."
else
    echo " -> port 53 is not exist."
    echo " -> $test: TEST-FAIL"
    if [ "$named_status" = "inactive" ]
    then
        exec_service_on_target named stop
    fi
    if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    exit
fi

if netstat -ln | grep ":953"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> port 953 is not exist."
    echo " -> $test: TEST-FAIL"
fi
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
