#!/bin/sh

# when the /var/named/ is transformed run the named checkzone command to verify that it is successfully terminated.

test="named_chkzone2"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
exec_service_on_target dnsmasq stop
exec_service_on_target named stop

if [ ! -d /var/named ]
then
    mkdir -p /var/named
fi

cp data/bind9/addr.arpa.db /var/named/$tst_bind_file
cp data/bind9/linux_test.com.db data/bind9/linux_test.com.db_bak
sed -i 's/remotehost/'"$remotehost"'/g' data/bind9/linux_test.com.db
cp data/bind9/linux_test.com.db /var/named/linux_test.com.db

if named-checkzone 192.168.0.0 /var/named/$tst_bind_file
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -f /var/named/$tst_bind_file /var/named/linux_test.com.db
mv data/bind9/linux_test.com.db_bak data/bind9/linux_test.com.db
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
