#!/bin/sh

# Start the named on target.
# Check the pid file.

test="named_pidfile"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)
exec_service_on_target dnsmasq stop
if [ -f /etc/bind/rndc.key ]
then
    rm -f /etc/bind/rndc.key
fi

exec_service_on_target named stop

sleep 5

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    if [ dnsmasq_status = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    exit
fi

if ls /var/run/named/named.pid
then
    echo " -> pid file of named is exist."
else
    echo " -> pid file of named is not exist."
    echo " -> $test: TEST-FAIL"
    if [ "$named_status" = "inactive" ]
    then
        exec_service_on_target named stop
    fi
    if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    exit
fi

if exec_service_on_target named stop
then
    echo " -> stop of named succeeded."
else
    echo " -> stop of named failed."
    echo " -> $test: TEST-FAIL"
    if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
    then
        exec_service_on_target dnsmasq start
    fi
    exit
fi

if test ! -f /var/run/named/named.pid
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
