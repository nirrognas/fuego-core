#!/bin/sh

#  In the target execute command rndc-confgen, and confirm the file rndc.key.

test="rndc-confgen"

named_status=$(get_service_status named)
dnsmasq_status=$(get_service_status dnsmasq)

exec_service_on_target dnsmasq stop
exec_service_on_target named stop

if [ -f /etc/bind/rndc.key ]
then
    mv /etc/bind/rndc.key /etc/bind/rndc.key_bak
fi

rndc-confgen -a -k rndckey -r /dev/urandom

echo "sleep 60 seconds"
sleep 60

if ls /etc/bind/rndc.key
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ -f /etc/bind/rndc.key_bak ]
then
    mv /etc/bind/rndc.key_bak /etc/bind/rndc.key
else
    rm -f /etc/bind/rndc.key
fi
if [ "$named_status" = "active" -o "$named_status" = "unknown" ]
then
    exec_service_on_target named start
fi
if [ "$dnsmasq_status" = "active" -o "$dnsmasq_status" = "unknown" ]
then
    exec_service_on_target dnsmasq start
fi
