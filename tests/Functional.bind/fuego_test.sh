NEED_ROOT=1

function test_pre_check {
    assert_has_program named-checkconf
    assert_has_program dig
    assert_has_program named
    assert_has_program netstat
    assert_has_program rndc-confgen
    assert_has_program named-checkzone
}

function test_deploy {
    put $TEST_HOME/bind_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    export tst_bind_file=$ADDR.in-addr.arpa.db;\
    export test_target_conf=$TARGET_ARCH;\
    export remotehost=$IPADDR;\
    ./bind_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
