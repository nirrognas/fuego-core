#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys
import common as plib

measurements = {}

regex_string = '^(Throughput)(.*)(MB/sec)(.*)(procs)$'
matches = plib.parse_log(regex_string)

if matches:
    measurements['default.dbench3'] = [{"name": "Throughput", "measure" : float(matches[0][1])}]

sys.exit(plib.process(measurements))
