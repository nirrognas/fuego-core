#!/bin/sh

#  In target, run command atmtcp to check the output of help.
#  option: -h

test="help"

atmtcp -h >atmtcp_log 2>&1
if grep [Uu]sage atmtcp_log ;
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm atmtcp_log
