NEED_ROOT=1

function test_pre_check {
    assert_has_program snmpget
    assert_has_program snmpgetnext
    assert_has_program snmpset
    assert_has_program snmpwalk
    assert_has_program snmptrap
    assert_has_program snmptranslate
}

function test_deploy {
    put $TEST_HOME/net-snmp_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    export remotehost=$IPADDR;\
    ./net-snmp_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
