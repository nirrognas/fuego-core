#!/bin/sh

#  In the target start snmptrapd, and check if the $snmptrapd_logfile
#  check the keyword "snmptrapd".

test="snmptrapd_syslog"

logger_service=$(detect_logger_service)

snmptrapd_status=$(get_service_status snmptrapd)
snmptrapd_logfile=$(get_service_logfile)
exec_service_on_target snmptrapd stop
exec_service_on_target $logger_service stop

if [ -f $snmptrapd_logfile ]
then
    mv $snmptrapd_logfile $snmptrapd_logfile"_bak"
fi

restore_target() {
    if [ -f $snmptrapd_logfile"_bak" ]
    then
        mv $snmptrapd_logfile"_bak" $snmptrapd_logfile
    fi
}

exec_service_on_target $logger_service restart

sleep 3

if exec_service_on_target snmptrapd start
then
    echo " -> start of snmptrapd succeeded."
else
    echo " -> start of snmptrapd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 3

if cat $snmptrapd_logfile | grep "snmptrapd"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$snmptrapd_status" = "inactive" ]
then
    exec_service_on_target snmptrapd stop
fi
restore_target
