#!/bin/sh

#  In the target start snmpd, and confirm the process condition by command ps.
#  check the keyword "snmpd".

test="snmpd_ps"

snmpd_status=$(get_service_status snmpd)

exec_service_on_target snmpd stop

if exec_service_on_target snmpd start
then
    echo " -> start of snmpd succeeded."
else
    echo " -> start of snmpd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps aux | grep "[/]usr/sbin/snmpd"
then
    echo " -> get the pid of snmpd."
else
    echo " -> can't get the pid of snmpd."
    echo " -> $test: TEST-FAIL"
    if [ "$snmpd_status" = "inactive" ]
    then
        exec_service_on_target snmpd stop
    fi
    exit
fi

if exec_service_on_target snmpd stop
then
    echo " -> stop of snmpd succeeded."
else
    echo " -> stop of snmpd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps aux | grep "[/]usr/sbin/snmpd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
if [ "$snmpd_status" = "active" -o "$snmpd_status" = "unknown" ]
then
    exec_service_on_target snmpd start
fi
