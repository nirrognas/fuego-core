function test_run {
    log_this "echo 1..6"
    log_this "echo \"ok 1 first test\""
    sleep 4
    log_this "echo \"ok 2 second test\""
    sleep 4
    log_this "echo \"ok 3 third test\""
    sleep 4
    log_this "echo \"ok 4 fourth test\""
    sleep 4
    log_this "echo \"ok 5 fifth test\""
    sleep 4

    log_this "echo waiting for 2 minutes, to give time for a timeout"
    sleep 60
    log_this "echo one minute"
    sleep 60
    log_this "echo two minutes"
    log_this "echo done sleeping - did I make it?"
    log_this "echo \"ok 6 sixth test\""
}

function test_processing {
    log_compare "$TESTDIR" "6" "^ok " "p"
}
