function test_pre_check {
    assert_define TEST_VERSION
    assert_define TEST_FUEGO_RELEASE
}

function test_run {
    log_this "echo This is version 1.0-1 of this test"
    log_this "echo TAP VERSION 1.3"
    log_this "echo 1..1"
    log_this "echo \"# TEST_VERSION=$TEST_VERSION\""
    log_this "echo \"# TEST_FUEGO_RELEASE=$TEST_FUEGO_RELEASE\""
    log_this "echo ok 1 Check for version and fuego release"
}

function test_processing {
    log_compare "$TESTDIR" "1" "^ok" "p"
}
