#!/bin/sh

#  Test command iperf3 on target.
#  Option : -c

test="client"

killall -KILL iperf3
iperf3 -s -D&

iperf3 -c 127.0.0.1 > log
sleep 5

if grep "connected" log
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

killall -KILL iperf3
rm log
