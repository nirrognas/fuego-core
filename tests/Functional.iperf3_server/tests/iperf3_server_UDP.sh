#!/bin/sh

#  Test command iperf3 on target.
#  Option : server(UDP)

test="UDP"

killall -KILL iperf3
iperf3 -s -D

if netstat -l | grep "5201"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

killall -KILL iperf3
