#!/bin/sh

#  Test command iperf3 on target.
#  Option : version

test="version"

if iperf3 --version | grep "iperf 3.*"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
