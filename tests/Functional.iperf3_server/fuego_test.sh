function test_pre_check {
    assert_has_program iperf3
    assert_has_program netstat
}

function test_deploy {
    put $TEST_HOME/iperf3_server_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    ./iperf3_server_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
