#!/bin/sh

#  In target, run command modinfo.
#  option: none

test="modinfo"

if modinfo $MODULE_NAME | grep "filename"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> module $MODULE_NAME does not exist."
    echo " -> $test: TEST-FAIL"
    exit
fi
