#!/usr/bin/python

import os, re, sys
import common as plib

ref_section_pat = "^\[[\w\d&._/()]+.[gle]{2}\]"

# 2715-T19C Linux 2.6.31.   12.0 8.6263   39.5   14.4   840.0 0.676 1.80210 5.002
# ("-" in the pattern of interest is to handle rare cases when Mhz is reprted as -1)
cur_search_pat = re.compile("^\S+\s+Linux\s[0-9a-z.-]+\s+([\s\d.-]*)",re.MULTILINE)

cur_dict = {}
cur_file = open(plib.TEST_LOG,'r')
print "Reading current values from " + plib.TEST_LOG +"\n"

lines = cur_file.readlines()

# Test group index
t_index = 0
sublist = []

print lines

for line in lines:
	result = cur_search_pat.findall(line)
	if result and len(result[0]) > 0:
		print 'result: ', result

		# Ugly hack to work around invalid processing of empty cells
		result[0] = result[0].replace('       ', '     0 ')

		test_res = result[0].rstrip('\n').split(' ')

                print "test_res = %s" % (test_res)

		if 0 < t_index < 8:
			for value in test_res:
				if len(value) > 0:
					sublist.append(value)
		t_index += 1

cur_file.close()

if len(sublist) == 0:
	sys.exit("\nparser: Fuego error reason: No results found\n")

cur_dict["Basic_Integer.Bit"] = sublist[0]
cur_dict["Basic_Integer.Add"] = sublist[1]
cur_dict["Basic_Integer.Mul"] = sublist[2]
cur_dict["Basic_Integer.Div"] = sublist[3]
cur_dict["Basic_Integer.Mod"] = sublist[4]
cur_dict["Basic_Float.Add"] = sublist[5]
cur_dict["Basic_Float.Mul"] = sublist[6]
cur_dict["Basic_Float.Div"] = sublist[7]
cur_dict["Basic_Float.Bogo"] = sublist[8]
cur_dict["Basic_Double.Add"] = sublist[9]
cur_dict["Basic_Double.Mul"] = sublist[10]
cur_dict["Basic_Double.Div"] = sublist[11]
cur_dict["Basic_Double.Bogo"] = sublist[12]
cur_dict["Context_switching.2p/0K"] = sublist[13]
cur_dict["Context_switching.2p/16K"] = sublist[14]
cur_dict["Context_switching.2p/64K"] = sublist[15]
cur_dict["Context_switching.8p/16K"] = sublist[16]
cur_dict["Context_switching.8p/64K"] = sublist[17]
cur_dict["Context_switching.16p/16K"] = sublist[18]
cur_dict["Context_switching.16p/64K"] = sublist[19]
cur_dict["Local_Communication_latencies.2p/0K"] = sublist[20]
cur_dict["Local_Communication_latencies.Pipe"] = sublist[21]
cur_dict["Local_Communication_latencies.AF_UNIX"] = sublist[22]
cur_dict["Local_Communication_latencies.UDP"] = sublist[23]
#cur_dict["Local_Communication_latencies.RPC_UDP"] = sublist[24]
cur_dict["Local_Communication_latencies.TCP"] = sublist[25]
#cur_dict["Local_Communication_latencies.RPC_TCP"] = sublist[26]
cur_dict["Local_Communication_latencies.TCP_conn"] = sublist[27]
cur_dict["File_&_VM_system_latencies_File_Create.0K"] = sublist[28]
cur_dict["File_&_VM_system_latencies_File_Delete.0K"] = sublist[29]
cur_dict["File_&_VM_system_latencies_File_Create.10K"] = sublist[30]
cur_dict["File_&_VM_system_latencies_File_Delete.10K"] = sublist[31]
cur_dict["File_&_VM_system_latencies_Mmap.Latency"] = sublist[32]
cur_dict["File_&_VM_system_latencies.Prot_Fault"] = sublist[33]
cur_dict["File_&_VM_system_latencies.Page_Fault"] = sublist[34]
cur_dict["File_&_VM_system_latencies.100fd_select"] = sublist[35]
cur_dict["Local_Communication_bandwidths.Pipe"] = sublist[36]
cur_dict["Local_Communication_bandwidths.AF_UNIX"] = sublist[37]
cur_dict["Local_Communication_bandwidths.TCP"] = sublist[38]
cur_dict["Local_Communication_bandwidths.File_reread"] = sublist[39]
cur_dict["Local_Communication_bandwidths.Mmap_reread"] = sublist[40]
cur_dict["Local_Communication_bandwidths_Bcopy.libc"] = sublist[41]
cur_dict["Local_Communication_bandwidths_Bcopy.hand"] = sublist[42]
cur_dict["Local_Communication_bandwidths_Mem.read"] = sublist[43]
cur_dict["Local_Communication_bandwidths_Mem.write"] = sublist[44]

print "cur_dist = %s" % (cur_dict)

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'xl', ' '))
