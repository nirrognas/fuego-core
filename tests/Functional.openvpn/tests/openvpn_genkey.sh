#!/bin/sh

#  In target, run command openvpn.
#  option: --genkey

test="genkey"

if [ -f /etc/openvpn/host-target.key ]
then
    mv /etc/openvpn/host-target.key /etc/openvpn/host-target.key_bak
fi

openvpn --genkey --secret /etc/openvpn/host-target.key
if [ -f /etc/openvpn/host-target.key ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm -f /etc/openvpn/host-target.key

if [ -f /etc/openvpn/host-target.key_bak ]
then
    mv /etc/openvpn/host-target.key_bak /etc/openvpn/host-target.key
fi
