function test_run {
    set +e
    run_test Functional.hello_world -s hello-fail
    SUB_RESULT=$?
    log_this "echo \"SUB_RESULT = $SUB_RESULT\""
    set -e
    if [ $SUB_RESULT == 0 ] ; then
        report "echo \"FAIL-FAILURE - Functional.hello_world succeeded, when it should have failed with spec 'hello-fail'\""
    else
        report "echo \"FAIL-OK - Functional.hello_world with spec 'hello-fail' failed as expected\""
    fi
}

function test_processing {
    log_compare "$TESTDIR" "1" "FAIL-OK" "p"
}
