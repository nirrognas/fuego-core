#!/bin/sh

# In target, run comannd batch and execute "echo" in batch
# Ensure that batch can work well.

test="batch"

if [ -f /tmp/batch_result ]
then
    rm /tmp/batch_result
fi

# execute script from file
batch < data/batch_test.sh

. ./fuego_board_function_lib.sh

set_init_manager

# As batch job is executed when cpu load is in lower level, so sometimes
# batch job will be pending.
# Restart atd service to ensure the batch job will execute immediately.
exec_service_on_target atd restart

sleep 61

if grep "batch test done" /tmp/batch_result
then
    echo ' -> $test: TEST-PASS'
else
    echo ' -> $test: TEST-FAIL'
fi

