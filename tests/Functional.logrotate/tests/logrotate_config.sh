#!/bin/sh

#  Turns on verbose mode and confirm the result.
#  option : -dv

test="test config"

if [ -f /var/lib/logrotate.status ]
then
    mv /var/lib/logrotate.status /var/lib/logrotate.status_bak
fi

cp data/test.conf /etc/logrotate.d/test.conf

chown root /etc/logrotate.d/test.conf

if logrotate -dv /etc/logrotate.d/test.conf
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ -f /var/lib/logrotate.status_bak ]
then
    mv /var/lib/logrotate.status_bak /var/lib/logrotate.status
fi
rm -f /etc/logrotate.d/test.conf
