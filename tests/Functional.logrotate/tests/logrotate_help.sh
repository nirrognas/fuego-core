#!/bin/sh

#  In the target to execute command logrotate and confirm the result.
#  option : --help

test="help"

if logrotate --help | grep "Usage"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
