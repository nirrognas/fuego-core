# This test grabs a number of pieces of information about a Fuego system
# It is intended to be run to diagnose the status of a remote lab, but
# can also be used to check the health of the current lab.
function test_run {
    log_this "echo TAP version 13"
    log_this "echo 1..1"

    # don't stop on test errors
    set +e
    # emit the version of this test, as diagnostic data
    # parse the version of this test
    log_this "echo \"# Version of this test=$TEST_VERSION-$TEST_FUEGO_RELEASE\""
    log_this "echo \"# Location of this test=$TEST_HOME\""

    # try checking the docker container for status of things
    testcase_num=1
    desc="process list of docker container"

    log_this "echo \"######### Process list of docker container:\""
    log_this "ps -aux"
    log_this "echo ok $testcase_num - $desc"

    # print git describe for fuego-core
    testcase_num=$(( testcase_num + 1 ))
    desc="git describe of fuego-core"

    log_this "echo \"######### git describe of fuego-core\" "
    pushd /fuego-core >/dev/null 2>&1
    log_this "git describe"
    popd >/dev/null 2>&1

    log_this "echo ok $testcase_num - $desc"

    # get a list of remotely installed tests:
    testcase_num=$(( testcase_num + 1 ))
    desc="remotely installed tests"

    log_this "echo \"######### Installed tests - outside of fuego-core\" "
    log_this "ls -l /fuego-rw/tests || true"
    log_this "echo ok $testcase_num - $desc"

    # print console log for last test run:
    testcase_num=$(( testcase_num + 1 ))
    desc="console log of last test run"

    last_test="$(ls -tr /fuego-rw/logs | grep -v fuego_lab_check | tail -n 1)"
    last_run="$(ls -tdr /fuego-rw/logs/$last_test/* | grep default | tail -n 1)"
    log_this "echo \"######### Console log of the last run\" "
    log_this "echo \"# last run=$last_run\""
    if [ -f $last_run/consolelog.txt ] ; then
        log_this "echo \"# Log:\""
        log_this "sed 's/^/# /' $last_run/consolelog.txt"
        log_this "echo \"######### end of log \" "
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi

    set -e
}
