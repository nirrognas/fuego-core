# Functional.filesytem_compare
#  This test checks to see if the filesystem is different from a 
#  a baseline snapshot taken some time in the past.
#
#  The purpose of this test is to do a high-level comparison of the 
#  contents of a filesystem, to see if anything has changed since
#  the last baseline snapshot was made.
#
# Usage:
#  preparation:
#    Create 2 jobs:
#     $ ftc add-job -b <brd> -t Functional.filesystem_compare -s default
#     $ ftc add-job -b <brd> -t Functional.filesystem_compare -s save_baseline
#    Save baseline reports
#      If the filesystem is in a known-good state that reflects "normal"
#      status for a board,
#      save the baseline data into a file by running run
#      the 'save_baseline' job:
#     $ ftc build-job <brd>.save_baseline.Functional.filesystem_compare
#      This will create baseline report files in /fuego-rw/boards/<brd>
# periodic usage:
#     Check that the overall reports still match, by running the default job:
#     $ ftc build-job <board>.default.Functional.filesystem_compare
#
# To do for this test:
# - omit timestamp field from run-failures
# suppress gathering current board state information
# FIXTHIS - function override from fuego_test.sh didn't work
#override-func ov_rootfs_state() {
#    return 0
#}

function test_pre_check {
    export board_rw_dir=$FUEGO_RW/boards/$NODE_NAME
    export baseline_file=$board_rw_dir/$TESTDIR-baseline-data.txt

    mkdir -p $board_rw_dir

    # check for existence of baseline file

    # but don't check if we're doing the save operation
    if [ "$TESTSPEC" = "save_baseline" ] ; then
        return 0
    fi

    if [ ! -f $baseline_file ] ; then
        echo "Missing baseline results file: $baseline_file1"
        echo "Maybe try running test with the 'save_baseline' spec?"
        abort_job "Missing baseline results file"
    fi
}

function test_run {
    echo "Getting filesystem data"

    DATA_FILE=$LOGDIR/filesystem-data.txt

    # get the contents of /bin
    cmd "ls -l /bin" >$DATA_FILE
    log_this "echo \"Here is the current filesystem data:\""
    log_this "cat $DATA_FILE"
    echo "--------"

    # if we're doing the "save_baseline" spec, save the data
    if [ "$TESTSPEC" = "save_baseline" ] ; then
        cp $DATA_FILE $baseline_file
        log_this "echo \"baseline file: $baseline_file saved\""
        log_this "echo \"ok 1 compare /bin contents and permissions baseline saved\""
        return 0
    fi

    # check for differences from baseline
    set +e
    log_this "echo \"Checking for differences in filesystem data\""
    log_this "diff -u $baseline_file $LOGDIR/filesystem-data.txt"
    diff_rcode="$?"
    log_this "echo ------------------------"
    set -e

    if [ $diff_rcode == "0" ] ; then
        log_this "echo \"no changes found between current filesystem and baseline\""
        log_this "echo \"ok 1 compare /bin contents and permissions\""
    fi
}

function test_processing {
    log_compare $TESTDIR 1 "^ok " "p"
}
