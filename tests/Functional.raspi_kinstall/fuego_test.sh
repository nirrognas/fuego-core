NEED_ROOT=1

function test_pre_check {
    # force a rebuild every execution
    if [ -e $JOB_BUILD_DIR/fuego_test_successfully_built ] ; then
        echo "Forcing rebuild"
        rm $JOB_BUILD_DIR/fuego_test_successfully_built
    fi

    # FIXTHIS - would be nice to check for no upstream change, but that's
    # a bit harder
}


function test_build {
    # this leaves zImage in arch/arm/boot,
    # and modules.tgz at the root of the build directory
    $TEST_HOME/rpi_code_checkout_build.sh
}

function test_deploy {
    # put kernel on target in /tmp
    put arch/arm/boot/zImage /tmp/kernel7.img
    put modules.tgz /tmp

    # extract into /boot and /
    cmd "cp /tmp/kernel7.img /boot/"
    cmd "cd /; tar -xzf /tmp/modules.tgz"
    cmd "sync;sync;sync"
    target_reboot

    sleep 60
}

function test_run {
    # actual test is just this: are we running the expected kernel?

    build_kver=$(strings vmlinux | grep "Linux version" | awk -F " " '{print $3}')

    log_this "echo Build kernel version: $build_kver"

    # get version of kernel running on board
    target_kver=$(cmd "uname -r")
    log_this "echo Kernel version on target: $target_kver"

    # Compare the required_version to current_version
    # FIXTHIS - this is weird - why the sort?
    if [ "$(printf '%s\n' "$target_kver" "$build_kver" | sort -V | head -n1)" = "$build_kver" ]; then
         log_this "echo ok 1 Check kernel version on board"
    else
         log_this "echo not ok 1 Check kernel version on board"
    fi
}

function test_processing {
    log_compare "$TESTDIR" "1" "^ok " "p"
}
