#!/bin/python

import os, re, sys
import common as plib

results = {}
with open(plib.TEST_LOG) as f:
    for line in f:
        if line.startswith("Running tests in"):
            test_set = line.split()[3]
            continue
        if line.startswith("selftests:"):
            fields = line.split()
            test_case = fields[1]
            results[test_set+'.'+test_case] = "PASS" if fields[2] == '[PASS]' else "FAIL"

sys.exit(plib.process(results))
