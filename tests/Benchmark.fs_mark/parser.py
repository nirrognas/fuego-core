#!/usr/bin/python

import os, re, sys
import common as plib

measurements = {}

print "Reading current values from " + plib.TEST_LOG + "\n"
log_lines = open(plib.TEST_LOG).readlines()
results = log_lines[-1].split()

if len(results) < 5:
    print "\nFuego error reason: Not enough results found\n"
    print "Line was: '%s'" % results
    sys.exit(2)

print "fs_mark raw results: " + str(results)

measurements["fs_mark"] = []

def add_measure(name, value):
    measurements["fs_mark"].append({"name": name, "measure":value})

add_measure("FSUse", int(results[0]))
add_measure("Count", int(results[1]))
add_measure("Size", int(results[2]))
add_measure("FS_Mark", float(results[3]))
add_measure("Overhead", int(results[4]))

sys.exit(plib.process(measurements))
