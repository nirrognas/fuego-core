#!/bin/sh

#  In the target start lvm2-monitor, and confirm the process condition by command ps.

test="ps"

. ./fuego_board_function_lib.sh

set_init_manager

service_status=$(get_service_status lvm2-monitor)

exec_service_on_target lvm2-monitor stop

if exec_service_on_target lvm2-monitor start
then
    echo " -> start of lvm2-monitor succeeded."
else
    echo " -> start of lvm2-monitor failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps aux | grep "[/]usr/sbin/lvmetad"
then
    echo " -> get the pid of lvmetad."
else
    echo " -> can't get the pid of lvmetad."
    echo " -> $test: TEST-FAIL"
fi
if [ "$service_status" = "inactive" ]
then
    exec_service_on_target lvm2-monitor stop
fi
