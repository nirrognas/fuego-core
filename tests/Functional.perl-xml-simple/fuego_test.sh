function test_pre_check {
    is_on_target_path perl PROGRAM_PERL
    assert_define PROGRAM_PERL "Missing 'perl' program on target board"
}

function test_deploy {
    put $TEST_HOME/perl-xml-simple_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        sh -v perl-xml-simple_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "1" "TEST-PASS" "p"
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
