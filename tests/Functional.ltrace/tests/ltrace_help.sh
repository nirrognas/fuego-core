#!/bin/sh

#  In target, run comannd ltrace to display help message.
#  option: -h/--help

test="help"

if ltrace -h | grep [uU]sage
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
