#!/bin/sh

#  The testscript checks the following options of the command file
#  1) Option no

test="detect_bash_file_type"

mkdir test_dir
touch test_dir/test1

cat >> test_dir/test1 <<EOF
#!/bin/bash
echo "this is test bash!"
EOF

if file test_dir/test1 | grep '.*Bourne-Again shell script.*text executable.*'
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
