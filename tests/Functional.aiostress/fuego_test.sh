tarball=aiostress.tar.gz

function test_pre_check {
    # check for libaio package on the board
    # also check for /lib/<something>/libaio<something) directly
    HAS_DPKG=$(cmd "dpkg --help >/dev/null ; echo \$?")
    if [ "$HAS_DPKG" == 0 ] ; then
        # FIXTHIS - this package-check is Debian-specific
        HAS_LIBAIO=$(cmd "dpkg -l libaio1 >/dev/null ; echo \$?")
    else
        HAS_LIBAIO=$(cmd "ls /lib/*/libaio* ; echo \$?")
    fi
    if [ "$HAS_LIBAIO" != "0" ] ; then
        abort_job "Package 'libaio1' (library libaio) is required and was not found on the board."
    fi
}

function test_build {
    $CC -I $SDKROOT/usr/include -L $SDKROOT/usr/lib  -Wall -lpthread -laio aiostress.c -o aiostress
}

function test_deploy {
    put aiostress  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    assert_define FUNCTIONAL_AIOSTRESS_MOUNT_BLOCKDEV
    assert_define FUNCTIONAL_AIOSTRESS_MOUNT_POINT
    assert_define FUNCTIONAL_AIOSTRESS_SIZE

    hd_test_mount_prepare $FUNCTIONAL_AIOSTRESS_MOUNT_BLOCKDEV $FUNCTIONAL_AIOSTRESS_MOUNT_POINT
    report "cd $FUNCTIONAL_AIOSTRESS_MOUNT_POINT/fuego.$TESTDIR; $BOARD_TESTDIR/fuego.$TESTDIR/aiostress -s $FUNCTIONAL_AIOSTRESS_SIZE ./testfile"

    hd_test_clean_umount $FUNCTIONAL_AIOSTRESS_MOUNT_BLOCKDEV $FUNCTIONAL_AIOSTRESS_MOUNT_POINT
}
