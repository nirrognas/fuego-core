OF.NAME="base-distrib"
OF.DESCRIPTION="Base distribution commands"

function ov_get_firmware() {
    local var

    var=$(cmd "uname -r") || abort_job "Unable to get firmware version"
    # remove leading and trailing whitespace, if any
    var="${var#"${var%%[![:space:]]*}"}"
    FW="${var%"${var##*[![:space:]]}"}"
}

function ov_rootfs_reboot() {
    cmd "/sbin/reboot &"
}

function ov_init_dir() {
    if [ -n "$1" ] ; then
        cmd "mkdir -p $1"
    fi
}

# can accept 1 or 2 arguments, indicating directories to be
# removed on the target.
# perform multiple removals in a single command, to avoid
# overhead
function ov_remove_and_init_dir() {
    if [ -n "$2" ] ; then
        cmd "rm -rf $1 ; mkdir -p $1 ; rm -rf $2 ; mkdir -p $2"
    else
        cmd "rm -rf $1 ; mkdir -p $1"
    fi
}

function ov_rootfs_state() {
    cmd "echo; uptime; echo; free; echo; df -h; echo; mount; echo; ps |grep -Fv \"  [\"; echo; cat /proc/interrupts; echo" || abort_job "Error executing rootfs_status operation on target"
}

function ov_logger() {
    cmd "logger $@" || abort_job "Could not execute ROOTFS_LOGGER command"
}

function ov_rootfs_sync() {
    cmd "sync" || abort_job "Unable to flush buffers on target"
}

function ov_rootfs_drop_caches() {
    cmd "echo 3 > /proc/sys/vm/drop_caches" || wprint "Unable to drop filesystem caches"
}

function ov_rootfs_oom() {
    cmd "echo 1000 > /proc/\$\$/oom_score_adj" || echo "WARNING: Unable to set OOM score adj."
}

# Kill any stale processes.
# First, issue normal kill, and finally, force its termination with signal 9.
# This cmd is basically doing a pkill.  It's done this
# way to avoid needing the 'pkill' command on the target.
# FIXTHIS: it would be nice to remove "Name:" from grep in ov_rootfs_kill
#   but I don't want to add a dependency on 'cut'
function ov_rootfs_kill() {
    for pat in "$@" ; do
        cmd "cd /proc ; for pid in [0-9]* ; do if cat /proc/\$pid/status | grep Name: | grep $pat >/dev/null 2>&1 ; then kill \$pid && sleep 2 && kill -9 \$pid ; fi ; done" || true
   done
}

# $1 - tmp dir, $2 = 'before' or 'after'
# create tmp dir ($1) if it doesn't already exist
function ov_rootfs_logread() {
    cmd "mkdir -p $1 && /sbin/logread > $1/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.$2" || abort_job "Error while ROOTFS_LOGREAD command execution on target"
}

# $1 - tmp dir, $2 = 'before' or 'after', $3 logdir
# create tmp dir ($1) if it doesn't already exist
function ov_rootfs_getlog() {
    get $1/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.$2 $3
}
